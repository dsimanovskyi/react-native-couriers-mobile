import React from 'react';
import {
  createBottomTabNavigator,
  createStackNavigator,
  createDrawerNavigator,
  createAppContainer,
  getActiveChildNavigationOptions,
} from 'react-navigation';
import { Icon } from 'react-native-elements';
import { defaultColor } from '../styles/palette';
import ProfileScreen from '../screens/ProfileScreen';
import MapOrdersScreen from '../screens/MapOrdersScreen';
import AuthScreen from '../screens/AuthScreen';
import OrdersListScreen from '../screens/OrdersListScreen';
import OrderDetail from '../screens/orderDetailScreen';
import ISListScreen from '../screens/ISListScreen';
import CashboxScreen from '../screens/cashboxScreen';

const SignedIn = createDrawerNavigator(
  {
    MainMap: {
      screen: createStackNavigator({
          MapIndex: {
            path: '/',
            screen: MapOrdersScreen,
          },
          OrderIndex: {
            path: '/:number',
            screen: OrderDetail
          },
        }, {
          initialRouteName: 'MapIndex'
        },
      ),
      navigationOptions: ({ navigation }) => (
        getActiveChildNavigationOptions(navigation)
      ),
    },
    Cashbox: {
      screen: createStackNavigator({
        CashboxScreen: {
          path: 'cashbox/',
          screen: CashboxScreen
        }
      }),
      navigationOptions: ({ navigation }) => (
        getActiveChildNavigationOptions(navigation)
      ),
    },
    Orders: {
      screen: createStackNavigator({
        ListScreen: {
          path: 'orders/',
          screen: OrdersListScreen
        },
        OrderDetail: {
          path: 'orders/:number',
          screen: OrderDetail
        }
      }),
      navigationOptions: ({ navigation }) => (
        getActiveChildNavigationOptions(navigation)
      ),
    },

    ISs: {
      screen: createStackNavigator({
        ListScreen: {
          path: 'IS/',
          screen: ISListScreen
        },
        MapIS: {
          path: 'IS/map',
          screen: MapOrdersScreen
        },
        OrderIS: {
          path: 'IS/order/:number',
          screen: OrderDetail
        },
      }, {
        initialRouteName: 'ListScreen',
      }),
      navigationOptions: () => {
        return {
          title: 'Марш. листы',
          drawerIcon: ({ tintColor }) => (
            <Icon name='description' size={25} color={tintColor} />
          )
        };
      },
    },
    Profile: {
      screen: createStackNavigator({
        ProfileScreen: {
          path: 'profile/',
          screen: ProfileScreen
        }
      }),
      navigationOptions: ({ navigation }) => (
        getActiveChildNavigationOptions(navigation)
      ),
    }
  },
  {
    initialRouteName: 'MainMap',
    drawerPosition: 'left',
    contentOptions: {
      activeTintColor: defaultColor,
    }
  }
);

const SignedOut = createBottomTabNavigator({
  auth: {
    screen: AuthScreen,
  }
});

export const appContainer = (signedIn = false) => createAppContainer(
  createStackNavigator(
    {
      SignedIn: {
        screen: SignedIn,
        navigationOptions: {
          gesturesEnabled: false
        }
      },
      SignedOut: {
        screen: SignedOut,
        navigationOptions: {
          gesturesEnabled: false
        }
      }
    },
    {
      headerMode: 'none',
      mode: 'modal',
      initialRouteName: signedIn ? 'SignedIn' : 'SignedOut'
    }
  )
);

