import { borderColor } from './palette';

export const centerFlexStyle = {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center'
};

export const borderShadowStyle = {
  backgroundColor: '#fff',
  borderWidth: 1,
  borderRadius: 4,
  borderColor,
  shadowColor: '#000',
  shadowOffset: { width: 0, height: 2 },
  shadowOpacity: 0.8,
  shadowRadius: 2,
};
