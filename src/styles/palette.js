export const defaultColor = '#1a9b9b';
export const secondaryColor = '#FF4081';
export const borderColor = '#e8e8e8';
export const separatorColor = '#b9b9b9';
export const textColorDefault = '#686868';
export const textTitleColorDefault = '#535353';
export const backgroundGroupColor = 'rgba(185, 185, 185, 0.1)';

// Tabs
export const tabsBackgroundColor = 'rgba(185, 185, 185, 0.3)';

// PopMenu
export const popMenuBackgroundColor = '#e8e8e8';
