import { defaultColor, borderColor } from './palette';

export const headerStyle = {
  backgroundColor: defaultColor,
};

export const headerTitleStyle = {
  color: borderColor,
  flex: 1,
  textAlign: 'center',
};
