import { _ } from 'lodash';
import {
  FETCH_LIST_ORDERS,
  FETCHING_DATA_ORDERS_LIST,
  UPDATE_ORDERS,
  IS_UPDATING_ORDER,
  DELIVER_ORDER,
  ADD_ORDER_TO_NOT_UPDATED,
  RETURN_PRODUCTS,
  CANCEL_RETURN_PRODUCTS,
  CANCEL_ORDER,
} from '../actions/types';

const INIT_STATE = {
  list: [],
  notUpdated: [],
};

export default function (state = INIT_STATE, action) {
  switch (action.type) {
    case FETCH_LIST_ORDERS:
      return { ...state, list: [...action.payload] };
    case FETCHING_DATA_ORDERS_LIST:
      return { ...state, isFetching: action.payload };
    case UPDATE_ORDERS:
      return { ...state, isUpdating: false, list: [...action.payload] };
    case IS_UPDATING_ORDER:
      return { ...state, isUpdating: action.payload };
    case DELIVER_ORDER:
      return deliverOrder(state, action.payload);
    case CANCEL_ORDER:
      return cancelOrder(state, action.payload);
    case ADD_ORDER_TO_NOT_UPDATED:
      return addOrderToNotUpdated(state, action.payload);
    case RETURN_PRODUCTS:
      return returnProducts(state, action.payload);
    case CANCEL_RETURN_PRODUCTS:
      return cancelReturnProducts(state, action.payload);
    default:
      return state;
  }
}

function deliverOrder(state, data) {
  const { orderId, status } = data;
  const list = _.cloneDeep(state.list);
  const index = list.findIndex(order => order.id === orderId);

  list[index].courierStatus = status;
  list[index].courierStatus.isCompleted = true;

  return { ...state, list };
}

function cancelOrder(state, data) {
  const { orderId, status } = data;
  const list = _.cloneDeep(state.list);
  const index = list.findIndex(order => order.id === orderId);

  list[index].courierStatus = status;
  list[index].courierStatus.isCompleted = true;

  return { ...state, list };
}

function addOrderToNotUpdated(state, orderId) {
  const { list } = state;
  const notUpdated = [...state.notUpdated];
  const targetOrder = list.find(order => order.id === orderId);

  notUpdated.push(targetOrder);

  return { ...state, notUpdated };
}

function returnProducts(state, orderData) {
  const { id, sum, owners, returnedProducts, returnedProductList } = orderData;
  const list = _.cloneDeep(state.list);
  const index = list.findIndex(order => order.id === id);

  list[index].sum = sum;
  list[index].owners = owners;
  list[index].returnedProducts = returnedProducts;
  list[index].returnedProductList = returnedProductList;

  return { ...state, list };
}

function cancelReturnProducts(state, orderData) {
  const { id, sum, owners, returnedProducts, returnedProductList } = orderData;
  const list = _.cloneDeep(state.list);
  const index = list.findIndex(order => order.id === id);

  list[index].sum = sum;
  list[index].owners = owners;
  list[index].returnedProducts = returnedProducts;
  list[index].returnedProductList = returnedProductList;

  return { ...state, list };
}
