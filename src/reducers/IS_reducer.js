import {
  FETCH_LIST_IS,
  FETCHING_DATA_IS_LIST
} from '../actions/types';

const INIT_STATE = { list: [] };

export default function (state = INIT_STATE, action) {
  switch (action.type) {
    case FETCH_LIST_IS:
      return { ...state, list: [...action.payload] };
    case FETCHING_DATA_IS_LIST:
      return { ...state, isFetching: action.payload };
    default:
      return state;
  }
}
