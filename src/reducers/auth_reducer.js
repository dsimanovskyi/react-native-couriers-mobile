import {
  AUTH_USER,
  PROFILE_USER,
  AUTH_ERROR,
  UNAUTH_USER,
  FETCH_AUTH_USER,
  FETCH_INIT_STATE_USER
} from '../actions/types';

export default function (state = { fetchAuth: false, fetchInitState: true }, action) {
    switch (action.type) {
        case AUTH_USER:
          return { ...state, error: '', authenticated: true };
        case FETCH_INIT_STATE_USER:
          return { ...state, fetchInitState: action.payload };
        case FETCH_AUTH_USER:
          return { ...state, fetchAuth: action.payload };
        case PROFILE_USER:
          return { ...state, error: '', currentUser: action.payload };
        case AUTH_ERROR:
          return { ...state, error: action.payload };
        case UNAUTH_USER:
          return { ...state, error: '', authenticated: false, currentUser: null };
        default:
          return state;
    }
}
