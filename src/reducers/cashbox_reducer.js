import {
  SET_RSO,
  SET_SHIFT,
  FETCHING_DATA_CASHBOX,
  SET_STOCK,
} from '../actions/types';

const INIT_STATE = {
  rso: null,
  shift: null,
  stock: null,
  isFetching: false
};

export default function (state = INIT_STATE, action) {
  switch (action.type) {
    case FETCHING_DATA_CASHBOX:
      return { ...state, isFetching: action.payload };
    case SET_RSO:
      return setRSO(state, action.payload);
    case SET_SHIFT:
      return setShift(state, action.payload);
    case SET_STOCK:
      return setStock(state, action.payload);
    default:
      return state;
  }
}

function setRSO(state, data) {
  let rso = {};

  if (!data || data.length === 0) {
    rso = {
      RSO: {
        code: null,
        serialNumber: null,
        BTAddress: null,
      },
      operator: {
        code: null,
        pwd: null,
      },
      dateReceived: null,
      cashboxNumber: null,
    };
  } else {
    const rsoData = data[0];
    const { RSO, courier } = rsoData;

    rso = {
      RSO: {
        code: RSO.code,
        serialNumber: RSO.serialNumber,
        BTAddress: RSO.BTAddress,
      },
      operator: {
        code: courier.operatorCode,
        pwd: courier.operatorPwd,
      },
      dateReceived: rsoData.dateReceived,
      cashboxNumber: rsoData.numberCashbox,
    };
  }

  return { ...state, rso };
}

function setShift(state, data) {
  let shift = null;

  if (data && data.constructor === Array) {
    shift = data[0] || null;
  } else if (data && data.updated) {
    shift = data.updated.closed === false ? data.updated : null;
  }

  return { ...state, shift };
}

function setStock(state, data) {
  let stock = null;

  if (data.constructor === Array) {
    const rso = data.find(item => item.paymentType === 'RSO');
    const cash = data.find(item => item.paymentType === 'cash');

    stock = {
      rso: rso.stock,
      cash: cash.stock,
    };
  }

  return { ...state, stock };
}
