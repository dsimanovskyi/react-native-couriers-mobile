import MapView from 'react-native-maps';

import {
  SET_CURRENT_GEOLOCATION,
  SET_REGION
} from '../actions/types';

const INIT_STATE = {
  location: {
    coords: {
      longitude: 30.4,
      latitude: 50.45,
      heading: 0,
    }
  },
  coordinate: new MapView.AnimatedRegion({
    latitude: 50.45,
    longitude: 30.4,
    longitudeDelta: 0.004,
    latitudeDelta: 0.009,
  }),
  region: {
    longitudeDelta: 0.004,
    latitudeDelta: 0.009,
    longitude: 30.4,
    latitude: 50.45
  },
  regionAnimated: new MapView.AnimatedRegion({
    longitudeDelta: 0.004,
    latitudeDelta: 0.009,
    longitude: 30.4,
    latitude: 50.45,
  })
};

export default function (state = INIT_STATE, action) {
  switch (action.type) {
    case SET_CURRENT_GEOLOCATION:
      return { ...state, location: action.payload };
    case SET_REGION:
      return { ...state, region: action.payload };
    default:
      return state;
  }
}
