import {
  PROCESS_MODAL,
} from '../actions/types';

const initialState = {
  processModal: {
    message: '',
    visible: false,
  }
};

export default function (state = initialState, action) {
  switch (action.type) {
    case PROCESS_MODAL:
      return processModal(state, action.payload);
    default:
      return state;
  }
}

function processModal(state, payload) {
  const processModal = Object.assign({}, state.processModal, payload);

  return { ...state, processModal };
}

