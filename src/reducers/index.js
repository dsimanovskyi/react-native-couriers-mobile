import { combineReducers } from 'redux';
import auth from './auth_reducer';
import geolocation from './geolocation_reducer';
import orders from './ordres_reducer';
import ISs from './IS_reducer';
import cashbox from './cashbox_reducer';
import modals from './modals_reducer';

export default combineReducers({
  auth,
  geolocation,
  orders,
  ISs,
  cashbox,
  modals,
});
