class ExellioCommandExecutorSingleton {
  constructor(BTAddress) {
    this._BTAddress = BTAddress;
  }

  async run(commands) {
    return { status: true };
  }
}

let instance = null;

export function ExellioCommandExecutor(BTAddress) {
  if (instance === null) {
    instance = new ExellioCommandExecutorSingleton(BTAddress);
  }

  return instance;
}
