import dev from './development';
import prod from './production';

export default __DEV__ ? dev : prod;
