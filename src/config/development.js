export default {
  get: (field) => devConfig[field]
};

const devConfig = {
  apiURL: 'http://192.168.3.160:3002/api',
  socketsURL: 'http://192.168.3.160:3002/'
  // apiURL: 'http://192.168.3.145:3002/api',
  // socketsURL: 'http://192.168.3.145:3002/'
};
