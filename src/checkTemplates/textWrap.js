export function textWrap(row, maxRowLength = 46) {
  if (row.length < maxRowLength) {
    return [row];
  }

  const rows = [];
  let currentRow = '';
  const words = row.split(' ');

  words.forEach(word => {
    if (currentRow.length + word.length < maxRowLength) {
      currentRow = currentRow + ` ${word}`;
    } else {
      rows.push(currentRow.trim());
      currentRow = word;
    }
  });

  if (currentRow !== '') {
    rows.push(currentRow.trim());
  }

  return rows;
}
