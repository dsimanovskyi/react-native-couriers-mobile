import { textWrap } from './textWrap';
import { formatDate, formatTime } from './formatDate';
import { numberToString } from '../helpers/numberToString';

const maxRowLength = 46;

export function requestForTransferTemplate(order, owner, copyNumber) {
  const header = getHeader(owner, copyNumber);
  const payer = getPayer(order.client);
  const recipient = getRecipient(order.number, owner.owner);
  const personalDataRegulations = getPersonalDataRegulations();
  const additionalDetails = getAdditionalDetails();

  return [
    ...header,
    ...payer,
    ...recipient,
    ...personalDataRegulations,
    ...additionalDetails,
  ];
}

function getHeader(owner, copyNumber) {
  const date = new Date();

  return [
    'ВПС ЕЛЕКТРУМ',
    '',
    'Заява на переказ готівки',
    `${'35077161'} екз ${copyNumber}`,
    '',
    ...textWrap(`Дата та час: ${formatDate(date)} ${formatTime(date)}`),
    '',
    ...textWrap(`${'ТОВ ЕПС 03131, м.Київ, вул.Столичне шосе, 103'}`),
    `${'ПНФП 16700081'}`,
    ...textWrap(`Адреса: ${'Київська КиївСвятошинський р-н., вул.Борщагівська, 154'}`),
    `Назва валюти: ${'UAH'}`,
    `Сума: ${owner.sum}`,
    'Еквівалент у грівнях:',
    ...textWrap(`${numberToString(owner.sum, true)}`),
    `Загальна сума: ${owner.sum}`,
    `Дт.рахунку: ${'3012'}`,
    `Кт.рахунку: ${'6851.1'}`,
  ];
}

function getPayer(client) {
  return [
    '',
    'ПЛАТНИК:',
    `${client.name}`,
    `Телефон: ${client.phones[0]}`,
  ];
}

function getRecipient(orderNumber, owner) {
  return [
    '',
    'ОТРИМУВАЧ:',
    ...textWrap(`${owner.name}`),
    `Телефон: ${owner.phone}`,
    ...textWrap(`Код отримувача: ${owner.edrpou}`, maxRowLength),
    ...textWrap(`Банк отримувача: ${owner.bank}`, maxRowLength),
    ...textWrap(`Код банку отримувача: ${owner.mfo}`, maxRowLength),
    ...textWrap(`Номер рахунку отримувача: ${owner.account}`, maxRowLength),
    'Призначення платежу:',
    ...textWrap(
      `Переказ коштів від фізічної особи через плтіжну систему. Оплата замовлення ${orderNumber}`,
      maxRowLength
    ),
  ];
}

function getPersonalDataRegulations() {
  return [
    '',
    ...textWrap(
      'Суб\'ект персональних даних повідомлений, що Персональні дані зазначені у цій заяві ' +
      'на переказ готівки, вносяться до персональних даних, володільцем якіх є ' +
      'ТОВ ЕПС (ЄДРПОУ 40243180 03131, м.Київ, вул.Столичне шосе, 103) ' +
      'безпосередньо під час здійснення операції з надання послуг з метою виконання вимог ' +
      'законодавства України щодо ідентифікації, а також про його права визначені ' +
      'Законом України "Про захист персональних даних". ' +
      'Передача персональних даних третім особам можлива тільки у випадку визначених ' +
      'чинним законодавством України.',
      maxRowLength
    )
  ];
}

function getAdditionalDetails() {
  return [
    '',
    'Додаткові реквізити:',
    '',
    ...textWrap(`Код операції: ${'35077161'}`),
    ...textWrap(`Унікальний код транзакції в ПС: ${'35077161'}`),
    '',
    `Підпис платника${'_'.repeat(15)}`,
    'Підпис фінансової установи:',
    `Крилова В.О.${'_'.repeat(15)}`,
    '',
    'Дякуємо, що скористались нашими послугами',
    'Зберігайте документ до зарахування коштів',
    `Контакт-центр ТОВ ЕПС: ${'+380(44)3000030'}`,
    ''
  ];
}
