export function textIndent(rows, length = 0, exceptFirstRow = false) {
  if (rows.constructor !== Array) {
    rows = [rows];
  }

  return rows.map((row, i) => {
    if (i === 0 && exceptFirstRow) {
      return row;
    }

    return ' '.repeat(length) + row;
  });
}
