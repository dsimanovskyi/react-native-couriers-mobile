import { textIndent } from './textIndent';

export function textToRight(text, maxRowLength, offset = 0) {
  if (typeof text !== 'string') {
    text = text.toString();
  }

  const indent = textIndent(maxRowLength - text.length - offset);

  return textIndent(text, indent);
}
