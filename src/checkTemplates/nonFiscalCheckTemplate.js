import { formatDate } from './formatDate';
import { textWrap } from './textWrap';
import { textIndent } from './textIndent';
import { textToRight } from './textToRight';
import { charReplacement } from './charReplacement';

const maxRowLength = 46;

export function nonFiscalCheckTemplate(order, owner) {
  const products = owner.products.filter(
    product => product.quantity > 0
  );
  const header = getHeader(order, owner);
  const body = getBody(products);
  const total = getTotal(owner);
  const footer = getFooter(owner, products);

  return [...header, ...body, ...total, ...footer];
}

function getHeader(order, owner) {
  const indent = 3;
  const wrappedRowLength = maxRowLength - indent;

  return [
    `Видаткова накладна #${order.number} / ${owner.isLTD ? 'ПФ' : 'МП'}`,
    `від ${formatDate(new Date())}.`,
    '',
    'Постачальник:',
    ...textIndent([
      ...textWrap(owner.owner.name, wrappedRowLength),
      ...textWrap(`Р/р ${owner.owner.account}, у банку ${owner.owner.bank}`, wrappedRowLength),
      ...textWrap(`МФО ${owner.owner.mfo}, код за ЄДРПОУ ${owner.owner.edrpou}`, wrappedRowLength)
    ], indent),
    'Покупець:',
    ...textIndent([
      ...textWrap(order.client.name, wrappedRowLength),
      ...textWrap(`Тел.: ${order.phone}`, wrappedRowLength),
    ], indent),
    `${'- '.repeat(23)}`
  ];
}

function getBody(products) {
  const indent = 3;
  const wrappedRowLength = maxRowLength - indent;
  let body = [];

  products.forEach((product, i) => {
    const dataStr = charReplacement(`${product.sku.name} ${product.quantity} шт. ${product.price} грн.`);
    const dataStrArr = textWrap(dataStr, wrappedRowLength);

    dataStrArr.push(textToRight(product.sum, maxRowLength, indent));

    body = [
      ...body,
      ...textIndent([
        ...textWrap(`${i + 1}. ${product.product.name}`, wrappedRowLength),
        ...dataStrArr,
      ], indent, true),
    ];
  });

  return body;
}

function getTotal(owner) {
  return [
    `${'- '.repeat(23)}`,
    `${textToRight(owner.sum, maxRowLength)}`,

    //todo: проверить значение ПДВ (что такое .vat)
    `${owner.isLtd ? 'У тому числі ПДВ' : ''}`
  ];
}

function getFooter(owner, products) {
  return [
    `Всього найменувань ${products.length} на суму ${owner.sum} грн.`,
    '',
    'Від виконавця *                   Отримав(ла)',
    '',
    '------------------           -----------------'
  ];
}
