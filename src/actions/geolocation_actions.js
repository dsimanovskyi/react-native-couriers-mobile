import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import {
  SET_CURRENT_GEOLOCATION,
  SET_REGION
} from './types';

export const getLocationAsync = () => async dispatch => {
  const { status } = await Permissions.askAsync(Permissions.LOCATION);

  if (status !== 'granted') {
    dispatch({ type: SET_CURRENT_GEOLOCATION, payload: null });
  }

  const location = await Location.getCurrentPositionAsync({});
  dispatch({ type: SET_CURRENT_GEOLOCATION, payload: location });
};

export const watchPosition = () => async dispatch => {
  setInterval(() => {
    navigator.geolocation.getCurrentPosition(
      position => {
        dispatch({
          type: SET_CURRENT_GEOLOCATION,
          payload: { coords: position.coords },
        });
      },
      error => {
        // dispatch({
        //   type: SET_CURRENT_GEOLOCATION,
        //   payload: null
        // });
      },
      {
        enableHighAccuracy: true,
        timeout: 10000,
        maximumAge: 10000,
      }
    );
  }, 1000);
};

export const setRegion = (region) => dispatch => {
  dispatch({ type: SET_REGION, payload: region });
};
