import { RequestManager } from '../helpers/RequestManager';
import {
  SET_RSO,
  SET_SHIFT,
  FETCHING_DATA_CASHBOX,
  SET_STOCK,
} from './types';

export const getRSO = () => async dispatch => {
  const response = await RequestManager.getRSO();

  if (response.status === true) {
    dispatch({ type: SET_RSO, payload: response.data });
  }
};

export const getShift = () => async dispatch => {
  dispatch({ type: FETCHING_DATA_CASHBOX, payload: true });

  const response = await RequestManager.getShift();

  dispatch({ type: FETCHING_DATA_CASHBOX, payload: false });

  if (response.status === true) {
    dispatch({ type: SET_SHIFT, payload: response.data });
  }
};

export const setShift = (shiftData) => async dispatch => {
  dispatch({ type: SET_SHIFT, payload: shiftData });
};

export const getStock = () => async dispatch => {
  const response = await RequestManager.getStock();

  if (response.status === true) {
    dispatch({ type: SET_STOCK, payload: response.data });
  }
};
