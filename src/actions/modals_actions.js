import { PROCESS_MODAL } from './types';

export const updateProcessModal = (data) => async dispatch => {
  dispatch({ type: PROCESS_MODAL, payload: data });
};
