import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import config from '../config';
import AlertError from '../components/AlertError';
import getErrorMessageResponse from '../utils/getErrorMessageResponse';
import updateList from '../utils/updateList';
import { RequestManager } from '../helpers/RequestManager';
import { errorAuth } from './auth_actions';
import {
  FETCH_LIST_ORDERS,
  FETCHING_DATA_ORDERS_LIST,
  UPDATE_ORDERS,
  IS_UPDATING_ORDER,
  DELIVER_ORDER,
  ADD_ORDER_TO_NOT_UPDATED,
  RETURN_PRODUCTS,
  CANCEL_RETURN_PRODUCTS,
  CANCEL_ORDER,
} from './types';

const ROOT_URL = config.get('apiURL');

export const getOrders = (courier) => async dispatch => {
  dispatch({ type: FETCHING_DATA_ORDERS_LIST, payload: true });
  try {
    const token = await AsyncStorage.getItem('token');
    const response = await axios.get(`${ROOT_URL}/orders/list?courier=${courier}`,
                                    { headers: { authorization: token } });
    dispatch({ type: FETCH_LIST_ORDERS, payload: response.data });
    dispatch({ type: FETCHING_DATA_ORDERS_LIST, payload: false });
  } catch (error) {
    dispatch({ type: FETCHING_DATA_ORDERS_LIST, payload: false });
    const errMessage = getErrorMessageResponse(error, 2001);
    errorAuth(dispatch, error.response);
    AlertError(errMessage);
  }
};

export const updateOrder = (id, list, body) => async dispatch => {
  dispatch({ type: IS_UPDATING_ORDER, payload: true });
  try {
    const token = await AsyncStorage.getItem('token');
    const response = await axios.put(`${ROOT_URL}/orders/${id}`, body,
                                    { headers: { authorization: token } });
    if (response) {
      const newList = updateList(list, '_id', body, id);
      dispatch({ type: UPDATE_ORDERS, payload: newList });
    }
  } catch (error) {
    dispatch({ type: IS_UPDATING_ORDER, payload: false });
    const errMessage = getErrorMessageResponse(error, 2002);
    errorAuth(dispatch, error.response);
    AlertError(errMessage);
  }
};

export const completeOrder = (orderId, data, status) => async dispatch => {
  dispatch({ type: DELIVER_ORDER, payload: { orderId, status } });

  return await RequestManager.updateOrder(orderId, data);
};

export const returnProducts = (data) => async dispatch => {
  dispatch({ type: RETURN_PRODUCTS, payload: data });
};

export const cancelReturnProducts = (data) => async dispatch => {
  dispatch({ type: CANCEL_RETURN_PRODUCTS, payload: data });
};

export const cancelOrder = (orderId, status) => async dispatch => {
  dispatch({ type: CANCEL_ORDER, payload: { orderId, status } });

  const data = { courierStatus: status._id };
  const response = await RequestManager.updateOrder(orderId, data);

  if (response.status === false) {
    dispatch({ type: ADD_ORDER_TO_NOT_UPDATED, payload: orderId });
  }
};
