import { BackHandler } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import AlertError from '../components/AlertError';
import getErrorMessageResponse from '../utils/getErrorMessageResponse';
import {
  AUTH_USER,
  PROFILE_USER,
  AUTH_ERROR,
  UNAUTH_USER,
  FETCH_AUTH_USER,
  FETCH_INIT_STATE_USER
} from '../actions/types';
import config from '../config';

const ROOT_URL = config.get('apiURL');

export const initStateUser = () => async (dispatch) => {
  dispatch({ type: FETCH_INIT_STATE_USER, payload: true });
  const token = await AsyncStorage.getItem('token');
  if (token) {
    await getProfile(dispatch, token);
    dispatch({ type: AUTH_USER });
  }
  dispatch({ type: FETCH_INIT_STATE_USER, payload: false });
};

export const signInUser = ({ username, password }) => async dispatch => {
  let signedIn = false;

  try {
    dispatch({ type: FETCH_AUTH_USER, payload: true });
    const { data } = await axios.post(`${ROOT_URL}/users/login?courier=true`,
                                      { username, password });
    if (data.success && data.token) {
      await AsyncStorage.setItem('token', data.token);
      await getProfile(dispatch, data.token);

      dispatch({ type: AUTH_USER });

      signedIn = true;
    } else {
      AsyncStorage.removeItem('token');
      AsyncStorage.removeItem('user');
      AsyncStorage.removeItem('userIsCashier');

      dispatch({ type: PROFILE_USER, payload: null });
    }
    dispatch({ type: FETCH_AUTH_USER, payload: false });

    return signedIn;
  } catch (error) {
    const errMessage = getErrorMessageResponse(error);
    dispatch({ type: FETCH_AUTH_USER, payload: false });
    dispatch(authError(errMessage));

    return signedIn;
  }
};

export const authError = (error) => ({
    type: AUTH_ERROR,
    payload: error
});

export const signoutUser = () => async dispatch => {
  await AsyncStorage.removeItem('token');
  await AsyncStorage.removeItem('user');
  await AsyncStorage.removeItem('userIsCashier');

  dispatch({ type: PROFILE_USER, payload: null });
  dispatch({ type: UNAUTH_USER });
  BackHandler.exitApp();
};

export const getProfile = async (dispatch, token = null) => {
  try {
    let currentToken = token;

    if (!currentToken) {
      currentToken = await AsyncStorage.getItem('token');
    }
    const { data } = await axios.post(`${ROOT_URL}/users/getProfile`, {}, {
                        headers: { authorization: currentToken }
                      });

    await AsyncStorage.setItem('user', data.userProfile.courier);

    if (data.userProfile.isCashier === true) {
      await AsyncStorage.setItem('userIsCashier', 'true');
    } else {
      await AsyncStorage.setItem('userIsCashier', 'false');
    }

    dispatch({ type: PROFILE_USER, payload: data.userProfile });
  } catch (error) {
    const errMessage = getErrorMessageResponse(error, 1001);
    errorAuth(dispatch, error.response);
    AlertError(errMessage);
  }
};

export const errorAuth = async (dispatch, response) => {
  if (response && response.data && response.data.error && response.data.error.auth_error) {
    await AsyncStorage.removeItem('token');
    await AsyncStorage.removeItem('user');
    await AsyncStorage.removeItem('userIsCashier');

    dispatch({ type: PROFILE_USER, payload: null });
    dispatch({ type: UNAUTH_USER });
  }
};
