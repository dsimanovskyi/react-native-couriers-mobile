// AUTH USERS
export const AUTH_USER = 'auth_user';
export const FETCH_AUTH_USER = 'fetch_auth_user';
export const FETCH_INIT_STATE_USER = 'fetch_init_state_user';
export const PROFILE_USER = 'profile_user';
export const AUTH_ERROR = 'auth_error';
export const UNAUTH_USER = 'unauth_user';

// GEOLOCATION
export const SET_CURRENT_GEOLOCATION = 'set_current_geolocation';
export const SET_REGION = 'set_region';

// ORDERS
export const FETCH_LIST_ORDERS = 'fetch_list_orders';
export const FETCHING_DATA_ORDERS_LIST = 'fetching_data_orders_list';
export const UPDATE_ORDERS = 'update_orders';
export const IS_UPDATING_ORDER = 'is_updating_order';
export const DELIVER_ORDER = 'deliver_order';
export const ADD_ORDER_TO_NOT_UPDATED = 'add_order_to_not_updated';
export const RETURN_PRODUCTS = 'return_products';
export const CANCEL_RETURN_PRODUCTS = 'cancel_return_products';
export const CANCEL_ORDER = 'cancel_order';

// IS
export const FETCH_LIST_IS = 'fetch_list_is';
export const FETCHING_DATA_IS_LIST = 'fetching_data_is_list';

// CASHBOX
export const SET_RSO = 'set_rso';
export const SET_SHIFT = 'set_shift';
export const SET_STOCK = 'set_stock';
export const FETCHING_DATA_CASHBOX = 'fetching_data_cashbox';

// MODALS
export const PROCESS_MODAL = 'process_modal';
