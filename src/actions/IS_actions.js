import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import config from '../config';
import AlertError from '../components/AlertError';
import getErrorMessageResponse from '../utils/getErrorMessageResponse';
import { errorAuth } from './auth_actions';
import {
  FETCH_LIST_IS,
  FETCHING_DATA_IS_LIST
} from './types';

const ROOT_URL = config.get('apiURL');

export const getISs = (courier) => async dispatch => {
  dispatch({ type: FETCHING_DATA_IS_LIST, payload: true });
  try {
    const token = await AsyncStorage.getItem('token');
    const response = await axios.get(`${ROOT_URL}/itinerary-sheet/list?courier=${courier}`,
                                    { headers: { authorization: token } });
    dispatch({ type: FETCH_LIST_IS, payload: response.data });
    dispatch({ type: FETCHING_DATA_IS_LIST, payload: false });
  } catch (error) {
    dispatch({ type: FETCHING_DATA_IS_LIST, payload: false });
    const errMessage = getErrorMessageResponse(error, 3001);
    errorAuth(dispatch, error.response);
    AlertError(errMessage);
  }
};
