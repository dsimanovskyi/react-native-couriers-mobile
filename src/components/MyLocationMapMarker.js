import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  Animated,
} from 'react-native';
import MapView from 'react-native-maps';
import isEqual from 'lodash/isEqual';
import { defaultColor } from '../styles/palette';

const ANCHOR = { x: 0.5, y: 0.5 };

const colorOfmyLocationMapMarker = defaultColor;

const propTypes = {
  ...MapView.Marker.Animated.propTypes,
  location: PropTypes.object.isRequired,
  children: PropTypes.node,
  heading: PropTypes.number,
  enableHack: PropTypes.bool,
};

const defaultProps = {
  enableHack: false,
};

export default class MyLocationMapMarker extends Component {

  componentWillUpdate(nextProps) {
    if (this.props.coordinate &&
      !isEqual(nextProps.location.coords, this.props.location.coords)) {
        this.props.coordinate.timing({
          latitude: nextProps.location.coords.latitude,
          longitude: nextProps.location.coords.longitude,
          duration: 1000
        }).start();
      }
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return this.props.location.coords.latitude !== nextProps.location.coords.latitude ||
           this.props.location.coords.longitude !== nextProps.location.coords.longitude;
  }

  render() {
    const { location, coordinate } = this.props;

    if (!location) return null;

    const heading = location.coords.heading;
    const rotate = (typeof heading === 'number' && heading >= 0) ? `${heading}deg` : null;

    return (
      <MapView.Marker.Animated
        anchor={ANCHOR}
        style={styles.mapMarker}
        {...this.props}
        coordinate={coordinate}
      >
        <Animated.View style={styles.container}>
          <Animated.View style={styles.markerHalo} />
          {rotate &&
            <Animated.View style={[styles.heading, { transform: [{ rotate }] }]}>
              <Animated.View style={styles.headingPointer} />
            </Animated.View>
          }
          <Animated.View style={styles.marker}>
            <Text style={{ width: 0, height: 0 }}>
              {this.props.enableHack && rotate}
            </Text>
          </Animated.View>
        </Animated.View>
        {this.props.children}
      </MapView.Marker.Animated>
    );
  }
}

const SIZE = 12;
const HALO_RADIUS = 7;
const ARROW_SIZE = 7;
const ARROW_DISTANCE = 6;
const HALO_SIZE = SIZE + HALO_RADIUS;
const HEADING_BOX_SIZE = HALO_SIZE + ARROW_SIZE + ARROW_DISTANCE;

const styles = StyleSheet.create({
  mapMarker: {
    zIndex: 1000,
  },
  // The container is necessary to protect the markerHalo shadow from clipping
  container: {
    width: HEADING_BOX_SIZE,
    height: HEADING_BOX_SIZE,
  },
  heading: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: HEADING_BOX_SIZE,
    height: HEADING_BOX_SIZE,
    alignItems: 'center',
  },
  headingPointer: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderTopWidth: 0,
    borderRightWidth: ARROW_SIZE * 0.75,
    borderBottomWidth: ARROW_SIZE,
    borderLeftWidth: ARROW_SIZE * 0.75,
    borderTopColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: colorOfmyLocationMapMarker,
    borderLeftColor: 'transparent',
  },
  markerHalo: {
    position: 'absolute',
    backgroundColor: colorOfmyLocationMapMarker,
    opacity: 0.3,
    top: 0,
    left: 0,
    width: HALO_SIZE,
    height: HALO_SIZE,
    borderRadius: Math.ceil(HALO_SIZE / 2),
    margin: (HEADING_BOX_SIZE - HALO_SIZE) / 2,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowRadius: 2,
    shadowOffset: {
      height: 0,
      width: 0,
    },
  },
  marker: {
    justifyContent: 'center',
    backgroundColor: colorOfmyLocationMapMarker,
    width: SIZE,
    height: SIZE,
    borderRadius: Math.ceil(SIZE / 2),
    margin: (HEADING_BOX_SIZE - SIZE) / 2,
  },
});

MyLocationMapMarker.propTypes = propTypes;
MyLocationMapMarker.defaultProps = defaultProps;
