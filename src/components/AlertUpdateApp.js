import { Alert } from 'react-native';

export default () => {
  Alert.alert(
    'Обновление приложения...',
    'Приложение будет перезапущено',
    [{ text: 'OK', onPress: () => {} }]
  );
};
