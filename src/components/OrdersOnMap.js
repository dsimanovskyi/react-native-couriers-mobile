import React, { Component } from 'react';
import { View } from 'react-native';
import OrderMarker from './OrderMarker';

export default class OrdersOnMap extends Component {

  renderMarkers() {
    if (this.props.listOrders.length === 0) {
      return null;
    }

    return this.props.listOrders.map((order) => {
      if (order.coordinates && order.coordinates.lng && order.coordinates.lat) {
        return (
          <OrderMarker
            onPressMarker={this.props.onPressMarker}
            key={order._id}
            order={order}
          />
        );
      }
      return null;
    });
  }

  render() {
    if (this.props.listOrders.length === 0) {
      return null;
    }

    return (
      <View>
        {this.renderMarkers()}
      </View>
    );
  }

}
