import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';

export class TouchableText extends Component {
  render() {
    const { onPress, style, text, disabled } = this.props;

    if (disabled === true) {
      return (
        <View>
          <Text style={ [style, styles.disabled] }>
            { text }
          </Text>
        </View>
      );
    }

    return (
      <TouchableOpacity onPress={ onPress }>
        <Text style={ [styles.baseStyle, style] }>
          { text }
        </Text>
      </TouchableOpacity>
    );
  }
}

TouchableText.propTypes = {
  onPress: PropTypes.func,
  style: Text.propTypes.style,
  text: PropTypes.string,
  disabled: PropTypes.bool,
};

const styles = StyleSheet.create({
  baseStyle: {
    color: '#3e6aa9',
    fontWeight: 'bold',
  },
  disabled: {
    color: '#939393',
    opacity: 0.5,
  }
});
