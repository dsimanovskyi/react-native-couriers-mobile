import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MapView from 'react-native-maps';
import { phonecall } from 'react-native-communications';
import { View, Text, StyleSheet } from 'react-native';
import PriceMarker from './PriceMarker';
import CustomCallout from './CustomCallout';

const propTypes = {
  order: PropTypes.object,
};

export default class OrderMarker extends Component {
  constructor(props) {
    super(props);

    this.handlePress = this.handlePress.bind(this);
  }

    shouldComponentUpdate(nextProps) {
      if (!nextProps.order.coordinates || !this.props.order.coordinates) {
        return false;
      }

      const shouldUpdate = nextProps.order.number !== this.props.order.number ||
                           nextProps.order.sum !== this.props.order.sum ||
                           nextProps.order.deliverySequence !== this.props.order.deliverySequence ||
                           nextProps.order.sequenceOrder !== this.props.order.sequenceOrder ||
                           nextProps.order.paymentSum !== this.props.order.paymentSum ||
                           nextProps.order.interval !== this.props.order.interval ||
                           nextProps.order.coordinates.lng !== this.props.order.coordinates.lng ||
                           nextProps.order.coordinates.lat !== this.props.order.coordinates.lat ||
                           nextProps.order.phone !== this.props.order.phone;
      return shouldUpdate;
    }

    callToClient() {
      if (this.props.order && this.props.order.phone) {
        phonecall(this.props.order.phone, true);
      }
    }

    handlePress() {
      const { order, onPressMarker } = this.props;

      if (onPressMarker) {
        onPressMarker(order.number);
      }
    }

    render() {
      const { order } = this.props;
      const toPay = order.sum - order.paymentSum;
      const sequenceOrder = order.sequenceOrder;

      if (order.coordinates && order.coordinates.lng && order.coordinates.lat) {
        let backgroundColor;
        let borderColor;

        if (order.interval === '9:00-13:00') {
          backgroundColor = '#1a9b5b';
          borderColor = '#187a49';
        } else if (order.interval === '19:00-22:00') {
          backgroundColor = '#219ca8';
          borderColor = '#09919f';
        } else if (order.interval === '14:00-18:00') {
          backgroundColor = '#e53974';
          borderColor = '#cc3367';
        }
        return (
          <MapView.Marker
            onPress={this.handlePress}
            coordinate={{ latitude: Number(order.coordinates.lat),
                          longitude: Number(order.coordinates.lng) }}
            tracksViewChanges={ false }
          >
            <PriceMarker
              amount={toPay}
              sequence={sequenceOrder}
              backgroundColor={backgroundColor}
              borderColor={borderColor}
            />
            {
              !this.props.onPressMarker ?
              <MapView.Callout
                tooltip
                style={styles.customView}
                onPress={this.callToClient.bind(this)}
              >
                <CustomCallout>
                  <View>
                    <Text>{order.number}</Text>
                    <Text>{order.phone}</Text>
                    <Text>{order.client ? order.client.name : '<Не указан>'}</Text>
                  </View>
                </CustomCallout>
              </MapView.Callout>
              :
              null
            }
          </MapView.Marker>
        );
      }
      return null;
    }
}

OrderMarker.propTypes = propTypes;

const styles = StyleSheet.create({
  customView: {
    width: 240,
    height: 100,
  }
});
