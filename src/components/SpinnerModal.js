import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  ActivityIndicator,
  Modal,
  View,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
} from 'react-native';

export class SpinnerModal extends Component {
  render() {
    const { visible, message } = this.props;

    return (
      <Modal
        transparent
        visible={ visible }
        onRequestClose={() => {}}
      >
        <TouchableWithoutFeedback>
          <View style={ styles.container }>
            <View style={ styles.dim } />
            <View style={ styles.content }>
              <Text style={ styles.text }>
                { message }
              </Text>
              <ActivityIndicator size='large' />
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

SpinnerModal.propTypes = {
  visible: PropTypes.bool,
  message: PropTypes.string,
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dim: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: '#000',
    opacity: 0.4,
  },
  content: {
    width: 300,
    height: 120,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    padding: 5,
    opacity: 1,
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
    paddingTop: 5,
    paddingBottom: 17,
  }
});
