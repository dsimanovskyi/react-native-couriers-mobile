import { Alert } from 'react-native';

export default (errorMessage, codeError = '') => {
  Alert.alert(
    `Ошибка ${codeError}`,
    errorMessage,
    [{ text: 'OK' }]
  );
};
