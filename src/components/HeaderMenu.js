import React from 'react';
import { Icon } from 'react-native-elements';
import { View } from 'react-native';
import { defaultColor } from '../styles/palette';

export function HeaderMenu(navigation) {
  return (
    <View style={ headerMenuStyle }>
      <Icon
        size={ 35 }
        name='menu'
        underlayColor={defaultColor}
        onPress={() => navigation.openDrawer()}
      />
    </View>
  );
}

const headerMenuStyle = {
  marginLeft: 20,
};
