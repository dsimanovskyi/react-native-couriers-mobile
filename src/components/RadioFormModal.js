import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Modal,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import RadioForm from 'react-native-simple-radio-button';
import { TouchableText } from './TouchableText';

export class RadioFormModal extends Component {
  constructor(props) {
    super(props);

    this.radioForm = React.createRef();
  }

  componentDidUpdate() {
    if (this.props.resetRadio === true && this.radioForm.current) {
      this.radioForm.current.updateIsActiveIndex(-1);
    }
  }

  render() {
    const { visible, radio, buttonBackIsDisabled, buttonFurtherIsDisabled } = this.props;
    const { onPressRadio, onRequestClose, onPressBack, onPressFurther } = this.props;

    return (
      <Modal
        transparent
        visible={ visible }
        onRequestClose={ onRequestClose }
      >
        <TouchableWithoutFeedback
          onPress={ onRequestClose }
        >
          <View style={ styles.container }>
            <TouchableWithoutFeedback>
              <View style={ styles.content }>
                <View style={ styles.options }>
                  <RadioForm
                    ref={ this.radioForm }
                    radio_props={ radio }
                    initial={ -1 }
                    onPress={ onPressRadio }
                    animation={ false }
                  />
                </View>
                <View style={ styles.actions }>
                  <TouchableText
                    onPress={ onPressBack }
                    style={ styles.actionsItem }
                    text='НАЗАД'
                    disabled={ buttonBackIsDisabled }
                  />
                  <TouchableText
                    onPress={ onPressFurther }
                    style={ styles.actionsItem }
                    text='ДАЛЕЕ'
                    disabled={ buttonFurtherIsDisabled }
                  />
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

RadioFormModal.propTypes = {
  visible: PropTypes.bool,
  radio: PropTypes.array,
  buttonBackIsDisabled: PropTypes.bool,
  buttonFurtherIsDisabled: PropTypes.bool,
  onPressRadio: PropTypes.func,
  onPressBack: PropTypes.func,
  onPressFurther: PropTypes.func,
  onRequestClose: PropTypes.func,
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    width: 350,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    padding: 5,
  },
  options: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 10,
  },
  actions: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 15,
    paddingRight: 15,
  },
  actionsItem: {
    color: '#3e6aa9',
    fontWeight: 'bold',
  }
});
