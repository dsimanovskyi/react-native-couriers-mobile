import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';

const propTypes = {
  amount: PropTypes.number.isRequired,
  sequence: PropTypes.number,
  fontSize: PropTypes.number,
};

const defaultProps = {
  backgroundColor: '#FF5A5F',
  borderColor: '#D23F44',
  fontSize: 13,
};

class PriceMarker extends React.Component {

  render() {
    const { fontSize, amount, backgroundColor, borderColor } = this.props;
    return (
      <View style={styles.container}>
        <View style={[styles.bubble, { backgroundColor, borderColor }]}>
          {
            this.props.sequence ?
            <View>
              <Text style={[styles.sequence]}>{this.props.sequence}</Text>
            </View>
            :
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.dollar}>₴</Text>
              <Text style={[styles.amount, { fontSize }]}>{amount}</Text>
            </View>
          }
        </View>
        <View style={[styles.arrowBorder, { borderTopColor: borderColor }]} />
        <View style={[styles.arrow, { borderTopColor: backgroundColor }]} />
      </View>
    );
  }
}

PriceMarker.propTypes = propTypes;
PriceMarker.defaultProps = defaultProps;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center'
  },
  bubble: {
    flex: 0,
    minWidth: 35,
    minHeight: 20,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: '#FF5A5F',
    padding: 2,
    borderRadius: 3,
    borderColor: '#D23F44',
    borderWidth: 0.5,
  },
  dollar: {
    color: '#FFFFFF',
    fontSize: 10,
  },
  amount: {
    color: '#FFFFFF',
    fontSize: 13,
  },
  sequence: {
    color: '#FFFFFF',
    fontSize: 17,
  },
  arrow: {
    backgroundColor: 'transparent',
    borderWidth: 4,
    borderColor: 'transparent',
    borderTopColor: '#FF5A5F',
    alignSelf: 'center',
    marginTop: -9,
  },
  arrowBorder: {
    backgroundColor: 'transparent',
    borderWidth: 4,
    borderColor: 'transparent',
    borderTopColor: '#D23F44',
    alignSelf: 'center',
    marginTop: -0.5,
  },
});

module.exports = PriceMarker;
