import React from 'react';
import {
  Modal,
  View,
  ViewPropTypes,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import { popMenuBackgroundColor, textTitleColorDefault } from '../../styles/palette';

export default class Menu extends React.PureComponent {

  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string,
      onPress: PropTypes.func
    })).isRequired,
    top: PropTypes.number,
    right: PropTypes.number,
    left: PropTypes.number,
    visible: PropTypes.bool,
    arrowPosition: PropTypes.oneOf(['topRight', 'topLeft', 'topCenter']),
    onVisible: PropTypes.func.isRequired,
    contentStyle: ViewPropTypes.style,
  }

  static defaultProps = {
    top: 10,
    arrowPosition: 'topRight',
    contentStyle: {
      backgroundColor: popMenuBackgroundColor,
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      visible: props.visible
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      visible: nextProps.visible
    });
  }

  dismiss = () => {
    this.setState({
      visible: false
    });
    this.props.onVisible(false);
  }

  renderItem = (item, index) =>
    (
      <View key={index} style={styles.item}>
        <TouchableOpacity
          style={{ flexDirection: 'row' }}
          onPress={() => {
            if (item.onPress) item.onPress();
            this.dismiss();
          }}
        >
          <View style={{ flex: 1 }} />
          <Text style={styles.text}>
            {item.title}
          </Text>
          <View style={{ flex: 1 }} />
        </TouchableOpacity>
        {
          (index === (this.props.data.length - 1)) ? null :
            <View style={styles.separator} />
        }
      </View>
    )

  renderContent = () => {
    const { data, left, contentStyle } = this.props;
    let { right } = this.props;
    const { top } = this.props;
    if (!right && !left) {
      right = 12;
    }

    // const arrowSize = 10;
    // let arrowLeft;
    // if (this.state.itemLayout) {
    //   switch (arrowPosition) {
    //     case 'topRight':
    //       arrowLeft = (this.state.itemLayout.x + this.state.itemLayout.width) - arrowSize - 8;
    //       break;
    //     case 'topLeft':
    //       arrowLeft = this.state.itemLayout.x + 8;
    //       break;
    //     case 'topCenter':
    //       arrowLeft =
    //         ((this.state.itemLayout.x + this.state.itemLayout.width) / 2.0) - (arrowSize / 2.0);
    //       break;
    //     default:
    //       break;
    //   }
    // }
    // <View
    //   style={[styles.arrow, {
    //     position: 'absolute',
    //     top: top - arrowSize,
    //     left: arrowLeft,
    //     borderBottomColor: contentStyle.backgroundColor,
    //   }]}
    // />
    return (
      <View style={{ flex: 1 }}>
        <View
          style={[styles.content, contentStyle, { top, right, left }]}
          onLayout={({ nativeEvent: { layout } }) => {
            this.setState({ itemLayout: layout });
          }}
        >
          {
            data.map((v, index) => (
              this.renderItem(v, index)
            ))
          }
        </View>
      </View>
    );
  }

  render() {
    return (
      <Modal
        animationType='none'
        transparent
        visible={this.state.visible}
        onRequestClose={this.dismiss}
      >
        <TouchableWithoutFeedback
          onPress={this.dismiss}
        >
          {this.renderContent()}
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    position: 'absolute',
    minWidth: 250,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    alignItems: 'center',
    overflow: 'hidden'
  },
  item: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
  },
  separator: {
    position: 'absolute',
    height: 0.5,
    left: 6,
    right: 6,
    bottom: 0,
    backgroundColor: '#ddd'
  },
  text: {
    fontSize: 17,
    color: textTitleColorDefault,
    paddingHorizontal: 30,
    paddingVertical: 15,
  },
  arrow: {
    width: 0,
    height: 0,
    borderStyle: 'solid',
    borderBottomWidth: 7,
    borderLeftWidth: 5,
    borderRightWidth: 5,
    borderTopWidth: 3,
    borderTopColor: '#f000',
    borderLeftColor: '#0f00',
    borderRightColor: '#00f0'
  }
});
