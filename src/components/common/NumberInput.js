import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TextInput, StyleSheet } from 'react-native';

export class NumberInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      text: this.numberToString(props.defaultValue),
    };
  }

  numberToString(value) {
    if (typeof value === 'number') {
      return value.toString();
    }

    return value;
  }

  handleInputChange = (text) => {
    const { onChange } = this.props;

    if (/^\d*$/.test(text)) {
      onChange(text);
      this.setState({ text });
    }
  };

  render() {
    const { placeholder, style } = this.props;

    return (
      <TextInput
        keyboardType='numeric'
        onChangeText={ this.handleInputChange }
        value={ this.state.text }
        placeholder={ placeholder }
        style={ [styles.baseStyle, style] }
      />
    );
  }
}

NumberInput.propTypes = {
  defaultValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  placeholder: PropTypes.string,
  style: TextInput.propTypes.style,
  onChange: PropTypes.func,
};

const styles = StyleSheet.create({
  baseStyle: {
    textAlign: 'center',
  }
});
