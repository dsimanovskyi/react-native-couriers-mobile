import React from 'react';
import PropTypes from 'prop-types';
import dateFormat from 'dateformat';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ListView
} from 'react-native';
import { SearchBar } from 'react-native-elements';
import { formateOnlyDate } from '../../constants/defaultDateFormate';
import {
  borderColor as borderColorDefault,
  separatorColor,
  textColorDefault,
  textTitleColorDefault,
  backgroundGroupColor
} from '../../styles/palette';

const SectionHeader = (props) => (
  <View style={stylesSectionHeader.container}>
    <Text style={stylesSectionHeader.text}>{props.groupBy}</Text>
  </View>
);

const Header = (props) => (
  <View style={stylesHeader.container}>

    <SearchBar
      lightTheme
      showLoading
      containerStyle={{ backgroundColor: 'rgba(185, 185, 185, 0.0)' }}
      inputStyle={{ backgroundColor: 'rgba(185, 185, 185, 0.5)' }}
      onChangeText={props.onChangeText}
      onClear={props.onChangeText}
      placeholder='Найти...'
    />
  </View>
);

const Footer = () => (
  <View style={styleFooter.container}>
    <TouchableOpacity style={styleFooter.button} onPress={() => console.log('load more')}>
      <Text style={styleFooter.text}>Load More</Text>
    </TouchableOpacity>
  </View>
);

const RenderFieldRows = (props) => {
  const { cardStyle, textStyle } = stylesRow;
  return props.fields.map((field) => {
    const fieldNesting = field.name.split('.');
    let fieldValue = fieldNesting.reduce(
      (prevValue, currentField) => prevValue[currentField],
      props
    );
    if (field.type === 'date') fieldValue = dateFormat(fieldValue, formateOnlyDate);
    if (field.type === 'price') fieldValue = `${fieldValue} ₴`;

    const styleValue = field.style ? field.style : {};
    const styleValueText = field.styleText ? field.styleText : {};

    return (
      <View key={field.name} style={[styleValue, cardStyle]}>
        <Text style={[styleValueText, textStyle]}>{fieldValue}</Text>
      </View>
    );
  });
};

const RenderGroupsRows = (props) =>
  props.gorups.map((group, index) =>
    (
      <View key={index} style={{ flex: 1, flexDirection: group.direction }}>
        <RenderFieldRows fields={group.fields} {...props} />
      </View>
    )
  );

const Row = (props) => {
  const { cardStyle, titleTextStyle, container } = stylesRow;
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      onPress={() => { if (props.onPressList) props.onPressList(props.title); }}
    >
      <View style={container}>
        <View style={cardStyle}>
          <Text style={titleTextStyle}>{props.title}</Text>
        </View>
        <RenderGroupsRows {...props} />
      </View>
    </TouchableOpacity>
  );
};

const propTypes = {
  data: PropTypes.array.isRequired,
  title: PropTypes.string,
  gorups: PropTypes.array,
  filter: PropTypes.string,
  groupByData: PropTypes.array,
  groupBy: PropTypes.string,
  onPressList: PropTypes.func,
};

class List extends React.Component {

  constructor(props) {
    super(props);

    const getSectionData = (dataBlob, sectionId) => dataBlob[sectionId];
    const getRowData = (dataBlob, sectionId, rowId) => dataBlob[`${rowId}`];

    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
      sectionHeaderHasChanged: (s1, s2) => s1 !== s2,
      getSectionData,
      getRowData,
    });

    const { dataBlob, sectionIds, rowIds } = this.formatData(
      this.props.data,
      this.props.groupByData,
      this.props.groupBy
    );

    this.state = {
      dataSource:
        this.props.groupBy ?
        this.ds.cloneWithRowsAndSections(dataBlob, sectionIds, rowIds) :
        this.ds.cloneWithRows(this.props.data),
    };
  }

  formatData(data, groupByData = [], field) {
    const dataBlob = {};
    const sectionIds = [];
    const rowIds = [];

    for (let sectionId = 0; sectionId < groupByData.length; sectionId++) {
      const currentGroupBy = groupByData[sectionId];

      const filtered = data.filter((dataRow) => dataRow[field] === currentGroupBy);

      if (filtered.length > 0) {
        sectionIds.push(sectionId);

        dataBlob[sectionId] = { groupBy: currentGroupBy };

        rowIds.push([]);

        for (let i = 0; i < filtered.length; i++) {
          const rowId = `${sectionId}:${i}`;
          rowIds[rowIds.length - 1].push(rowId);
          dataBlob[rowId] = filtered[i];
        }
      }
    }

    return { dataBlob, sectionIds, rowIds };
  }

  filterData(text) {
    const fieldFilter = this.props.filter;
    const filtered = this.props.data.filter(row =>
                      row[fieldFilter].toLowerCase().indexOf(text) !== -1);
    const { dataBlob, sectionIds, rowIds } = this.formatData(
      filtered,
      this.props.groupByData,
      this.props.groupBy
    );
    this.setState({
      dataSource:
        this.props.groupBy ?
        this.ds.cloneWithRowsAndSections(dataBlob, sectionIds, rowIds) :
        this.ds.cloneWithRows(filtered),
    });
  }

  render() {
    return (
      <ListView
        style={styles.container}
        dataSource={this.state.dataSource}
        renderRow={
          (data) => (<Row
            onPressList={this.props.onPressList}
            gorups={this.props.gorups}
            title={data[this.props.title]} {...data}
          />)
        }
        renderHeader={() =>
          <Header onChangeText={this.filterData.bind(this)} />
        }
        renderFooter={() => false ? <Footer /> : null}
        renderSectionHeader={(sectionData) =>
          this.props.groupBy ? <SectionHeader {...sectionData} /> : null}
      />
    );
  }

}

//renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}

List.propTypes = propTypes;

export { List };

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: separatorColor,
  },
});

const stylesHeader = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 10
  },
});

const stylesRow = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
    marginVertical: 3,
    backgroundColor: '#f2f2f2',
  },
  textStyle: {
    fontSize: 14,
    color: textColorDefault,
  },
  titleTextStyle: {
    fontSize: 18,
    color: textTitleColorDefault,
  },
  cardStyle: {
    flex: 1,
    marginBottom: 5,
  }
});


const styleFooter = StyleSheet.create({
  container: {
    flex: 1,
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    borderColor: borderColorDefault,
    borderWidth: StyleSheet.hairlineWidth,
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
  },
  text: {
    color: '#8E8E8E',
  },
});

const stylesSectionHeader = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 3,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: backgroundGroupColor,
  },
  text: {
    fontSize: 17,
  },
});
