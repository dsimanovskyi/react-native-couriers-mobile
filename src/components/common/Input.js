import React from 'react';
import { View } from 'react-native';
import { FormLabel, FormInput } from 'react-native-elements';

const Input = ({ label, value, onChangeText, placeholder, secureTextEntry }) => {
  const { inputStyle, labelStyle, containerStyle } = styles;

  return (
    <View style={containerStyle}>
      <FormLabel labelStyle={labelStyle}>{label}</FormLabel>
      <FormInput
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}
        inputStyle={inputStyle}
        value={value}
        onChangeText={onChangeText}
      />
    </View>
  );
};

const styles = {
  inputStyle: {
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 14,
    lineHeight: 23,
  },
  labelStyle: {
    fontSize: 14,
  }
};

export { Input };
