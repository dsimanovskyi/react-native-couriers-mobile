import React from 'react';
import { Icon } from 'react-native-elements';
import { TouchableOpacity } from 'react-native';

export function HeaderServiceMenu({ onPressMenu }) {
  return (
    <TouchableOpacity
      style={{ paddingRight: 10, paddingLeft: 20 }}
      onPress={ onPressMenu }
    >
      <Icon name='more-vert' />
    </TouchableOpacity>
  );
}
