import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Text } from 'react-native';
import { centerFlexStyle } from '../styles/common';

export class Message extends Component {
  render() {
    const { message } = this.props;

    return (
      <View style={ [centerFlexStyle, styles.container] }>
        <Text style={ styles.text }>
          { message }
        </Text>
      </View>
    );
  }
}

Message.propTypes = {
  message: PropTypes.string.isRequired,
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
  }
});
