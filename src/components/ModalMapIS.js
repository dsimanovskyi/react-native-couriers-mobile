import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import Modal from 'react-native-modal';
import { defaultColor } from '../styles/palette';

const propTypes = {
  isModalVisible: PropTypes.bool.isRequired,
  _toggleModal: PropTypes.func.isRequired,
  _setSequenceOrders: PropTypes.func,
  _setStartIS: PropTypes.func,
};

class ModalMapIS extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Modal
          isVisible={this.props.isModalVisible}
          style={styles.bottomModal}
          onBackdropPress={this.props._toggleModal}
        >
          <View style={styles.modalContent}>
            <Button
              large
              onPress={this.props._setSequenceOrders}
              title='Установить последовательность'
              buttonStyle={styles.buttonStyle}
              containerViewStyle={styles.buttonContainer}
            />
            <Button
              large
              onPress={this.props._setStartIS}
              title='Выехать на маршрут'
              buttonStyle={styles.buttonStyle}
              containerViewStyle={styles.buttonContainer}
              backgroundColor={defaultColor}
            />
          </View>
        </Modal>
      </View>
    );
  }
}

ModalMapIS.propTypes = propTypes;

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 5
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)'
  },
  buttonContainer: {
    alignSelf: 'stretch',
    marginTop: 10,
  },
  buttonStyle: {
    borderRadius: 5,
  }
});

export default ModalMapIS;
