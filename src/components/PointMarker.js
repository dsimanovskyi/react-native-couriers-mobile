import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
} from 'react-native';
import { defaultColor } from '../styles/palette';

const propTypes = {
  fontSize: PropTypes.number,
};

const defaultProps = {
  fontSize: 13,
};

class PriceMarker extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.point} />
        <View style={styles.arrow} />
      </View>
    );
  }
}

PriceMarker.propTypes = propTypes;
PriceMarker.defaultProps = defaultProps;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignSelf: 'flex-start',
  },
  point: {
    flex: 0,
    width: 10,
    height: 10,
    backgroundColor: defaultColor,
    borderRadius: 5,
  }
});

module.exports = PriceMarker;
