import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import dateFormat from 'dateformat';
import { formateDateWithSecond } from '../../constants/defaultDateFormate';

export class InfoBlock extends Component {
  formatDate(date) {
    const userDate = new Date(date);
    // const userTimezoneOffset = userDate.getTimezoneOffset() * 60 * 1000;
    // const fixedDate = new Date(userDate.getTime() + userTimezoneOffset);

    return dateFormat(userDate, formateDateWithSecond);
  }

  rsoInfo(rso) {
    if (!rso) return null;

    const { RSO, dateReceived } = rso;

    return (
      <React.Fragment>
        <Text>
          РРО: { RSO.code } / { RSO.serialNumber }
        </Text>
        <Text>
          Получен: { dateReceived && this.formatDate(dateReceived) }
        </Text>
      </React.Fragment>
    );
  }

  shiftInfo(shift) {
    if (!shift) return null;

    const { dateOpened } = shift;

    return (
      <Text>
        Дата открытия смены: { this.formatDate(dateOpened) }
      </Text>
    );
  }

  render() {
    const { rso, shift } = this.props;

    return (
      <View style={styles.textArea}>
        { this.rsoInfo(rso) }
        { this.shiftInfo(shift) }
      </View>
    );
  }
}

InfoBlock.propTypes = {
  rso: PropTypes.shape({
    code: PropTypes.string,
    serialNumber: PropTypes.string,
    dateReceived: PropTypes.string,
  }),
  shift: PropTypes.shape({
    dateOpened: PropTypes.string,
  }),
};

const styles = {
  textArea: {
    marginLeft: 25,
    marginTop: 25,
  },
};

