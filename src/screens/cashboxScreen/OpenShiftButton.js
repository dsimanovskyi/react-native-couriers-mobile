import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-native-elements';
import { RequestManager } from '../../helpers/RequestManager';
import alertError from '../../components/AlertError';
import { ExellioCommandExecutor as Exellio } from '../../exellioDriver/ExellioCommandExecutor';
import { REQUEST_TO_API, PRINT_X_REPORT } from '../../constants/messages';

export class OpenShiftButton extends Component {
  constructor(props) {
    super(props);

    this.handlePress = this.handlePress.bind(this);
  }

  handlePress() {
    const { user, openShift, updateProcessModal } = this.props;

    updateProcessModal({
      message: PRINT_X_REPORT,
      visible: true,
    });

    this.printXreport()
      .then(report => {
        if (report.status === true) {
          updateProcessModal({ message: REQUEST_TO_API });

          RequestManager.openShift()
            .then(response => {
              if (response.status === true) {
                openShift(response.data);
              }

              updateProcessModal({ visible: false });
            });
        } else {
          updateProcessModal({ visible: false });

          alertError(report.error, report.errorCode);

          const errorReport = {
            code: report.errorCode,
            user: user.courier,
            type: 'RSO',
            title: report.error,
            description: report,
          };

          RequestManager.logError(errorReport);
        }
      });
  }

  printXreport() {
    const { RSO, operator } = this.props.rso;
    const exellio = Exellio(RSO.BTAddress);
    const cmd = { name: 'xReport', data: operator.pwd };

    return exellio.run(cmd);
  }

  render() {
    const { style, color, disabled } = this.props;

    return (
      <Button
        large
        buttonStyle={ style }
        backgroundColor={ color }
        title='Открыть смену'
        onPress={ this.handlePress }
        disabled={ disabled }
      />
    );
  }
}

OpenShiftButton.propTypes = {
  style: PropTypes.object,
  color: PropTypes.string,
  disabled: PropTypes.bool,
  user: PropTypes.object,
  openShift: PropTypes.func,
  updateProcessModal: PropTypes.func,
  rso: PropTypes.object,
};
