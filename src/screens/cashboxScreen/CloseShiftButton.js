import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-native-elements';
import { RequestManager } from '../../helpers/RequestManager';
import alertError from '../../components/AlertError';
import { ExellioCommandExecutor as Exellio } from '../../exellioDriver/ExellioCommandExecutor';
import { REQUEST_TO_API, PRINT_Z_REPORT } from '../../constants/messages';

export class CloseShiftButton extends Component {
  constructor(props) {
    super(props);

    this.handlePress = this.handlePress.bind(this);
  }

  handlePress() {
    const { user, closeShift, updateProcessModal } = this.props;

    updateProcessModal({
      message: PRINT_Z_REPORT,
      visible: true
    });

    this.printZreport()
      .then(report => {
        if (report.status === true) {
          updateProcessModal({ message: REQUEST_TO_API });

          RequestManager.closeShift()
            .then(response => {
              if (response.status === true) {
                closeShift(response.data);
              }

              updateProcessModal({ visible: false });
            });
        } else {
          updateProcessModal({ visible: false });

          alertError(report.error, report.errorCode);

          const errorReport = {
            code: report.errorCode,
            user: user.courier,
            type: 'RSO',
            title: report.error,
            description: report,
          };

          RequestManager.logError(errorReport);
        }
      });
  }
  
  printZreport() {
    const { RSO, operator } = this.props.rso;
    const exellio = Exellio(RSO.BTAddress);
    const cmd = { name: 'zReport', data: operator.pwd };

    return exellio.run(cmd);
  }

  render() {
    const { style, color, disabled } = this.props;

    return (
      <Button
        large
        buttonStyle={ style }
        backgroundColor={ color }
        title='Закрыть смену'
        onPress={ this.handlePress }
        disabled={ disabled }
      />
    );
  }
}

CloseShiftButton.propTypes = {
  style: PropTypes.object,
  color: PropTypes.string,
  disabled: PropTypes.bool,
  user: PropTypes.object,
  closeShift: PropTypes.func,
  updateProcessModal: PropTypes.func,
  rso: PropTypes.object,
};
