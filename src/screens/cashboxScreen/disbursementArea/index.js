import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { RequestManager } from '../../../helpers/RequestManager';
import { ExellioCommandExecutor as Exellio } from '../../../exellioDriver/ExellioCommandExecutor';
import alertError from '../../../components/AlertError';
import { REQUEST_TO_API, REQUEST_TO_RSO } from '../../../constants/messages';
import { DisbursementModal } from './DisbursementModal';
import { DisbursementButtons } from './DisbursementButtons';

export class DisbursementArea extends Component {
  constructor(props) {
    super(props);

    this.paymentType = null;
    this.state = {
      disbursementModalIsVisible: false,
    };

    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handlePost = this.handlePost.bind(this);
  }

  handleOpenModal(paymentType) {
    this.paymentType = paymentType;

    this.setState({
      disbursementModalIsVisible: true,
    });
  }

  handleCloseModal() {
    this.setState({
      disbursementModalIsVisible: false,
    });
  }


  handlePost(inputValue) {
    const { rso, stock, courier, getStock, updateProcessModal } = this.props;

    let fixedValue = Math.abs(parseFloat(inputValue));

    if (fixedValue > stock[this.paymentType]) {
      fixedValue = stock[this.paymentType];
    }

    const disbursementData = {
      courier,
      numberCashbox: rso.cashboxNumber,
      paymentType: this.paymentType === 'rso' ? 'RSO' : this.paymentType,
      sum: fixedValue,
    };

    if (this.paymentType === 'rso') {
      updateProcessModal({
        message: REQUEST_TO_RSO,
        visible: true,
      });

      this.withdrawFunds(fixedValue)
        .then(report => {
          if (report.status === true) {
            updateProcessModal({ message: REQUEST_TO_API });

            RequestManager.disbursement(disbursementData)
              .then(response => {
                if (response.status === true) {
                  getStock();
                }

                updateProcessModal({ visible: false });
              });
          } else {
            updateProcessModal({ visible: false });

            alertError(report.error, report.errorCode);

            const errorReport = {
              code: report.errorCode,
              user: courier,
              type: 'RSO',
              title: report.error,
              description: report,
            };

            RequestManager.logError(errorReport);
          }
        });
    } else {
      updateProcessModal({
        message: REQUEST_TO_API,
        visible: true,
      });

      RequestManager.disbursement(disbursementData)
        .then(response => {
          if (response.status === true) {
            getStock();
          }

          updateProcessModal({ visible: false });
        });
    }
  }

  withdrawFunds(amount) {
    const { RSO, operator, cashboxNumber } = this.props.rso;
    const exellio = Exellio(RSO.BTAddress);
    const cmd = {
      name: 'withdrawFunds',
      data: {
        operNumber: operator.code,
        operPwd: operator.pwd,
        tillNumber: cashboxNumber,
        amount: -amount,
      }
    };

    return exellio.run(cmd);
  }

  renderModal() {
    if (!this.paymentType) return null;

    const { disbursementModalIsVisible } = this.state;
    const { stock } = this.props;

    return (
      <DisbursementModal
        visible={ disbursementModalIsVisible }
        inputValue={ stock[this.paymentType] }
        onRequestClose={ this.handleCloseModal }
        onFinish={ this.handlePost }
      />
    );
  }

  render() {
    const { shift, stock } = this.props;

    if (!stock) return null;

    return (
      <React.Fragment>
        <DisbursementButtons
          shift={ shift }
          stock={ stock }
          onPress={ this.handleOpenModal }
        />
        { this.renderModal() }
      </React.Fragment>
    );
  }
}

DisbursementArea.propTypes = {
  rso: PropTypes.object,
  stock: PropTypes.object,
  shift: PropTypes.object,
  courier: PropTypes.string,
  getStock: PropTypes.func,
  updateProcessModal: PropTypes.func,
};
