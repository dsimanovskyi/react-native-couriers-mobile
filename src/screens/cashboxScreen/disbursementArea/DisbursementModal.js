import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Modal,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import { NumberInput } from '../../../components/common/NumberInput';
import { TouchableText } from '../../../components/TouchableText';

export class DisbursementModal extends Component {
  constructor(props) {
    super(props);

    this.inputValue = props.inputValue;

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handlePress = this.handlePress.bind(this);
  }

  handleInputChange(value) {
    this.inputValue = value;
  }

  handlePress() {
    const { onRequestClose, onFinish } = this.props;

    onFinish(this.inputValue);
    onRequestClose();
  }

  render() {
    const { inputValue, visible, onRequestClose } = this.props;

    return (
      <Modal
        transparent
        visible={ visible }
        onRequestClose={ onRequestClose }
      >
        <TouchableWithoutFeedback
          onPress={ onRequestClose }
        >
          <View style={ styles.container }>
            <View style={ styles.dim } />
            <View style={ styles.content }>
              <View style={ styles.item }>
                <NumberInput
                  defaultValue={ inputValue }
                  onChange={ this.handleInputChange }
                  style={ styles.numberInput }
                />
                <TouchableText
                  text='OK'
                  onPress={ this.handlePress }
                  style={ styles.touchableText }
                />
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

DisbursementModal.propTypes = {
  visible: PropTypes.bool,
  inputValue: PropTypes.number,
  onRequestClose: PropTypes.func,
  onFinish: PropTypes.func,
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dim: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: '#000',
    opacity: 0.4,
  },
  content: {
    width: 250,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    padding: 5,
  },
  item: {
    height: 75,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  numberInput: {
    borderColor: '#b1b1b1',
    borderWidth: 1,
    width: 140,
    fontSize: 22,
    height: 50,
    marginRight: 25,
  },
  touchableText: {
    fontSize: 25,
  }
});
