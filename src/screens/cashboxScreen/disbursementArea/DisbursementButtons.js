import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View } from 'react-native';
import { TouchableText } from '../../../components/TouchableText';

export class DisbursementButtons extends Component {
  renderTouchable(paymentType) {
    const { shift, onPress } = this.props;

    if (!shift) return null;

    return (
      <TouchableText
        style={ [styles.text, styles.touchable] }
        text='ВЫДАЧА СРЕДСТВ'
        onPress={ () => onPress(paymentType) }
      />
    );
  }
  
  render() {
    const { stock } = this.props;

    return (
      <View style={ styles.container }>
        <Text style={ styles.header }>
          Остатки по кассе:
        </Text>
        <View style={ styles.row }>
          <View>
            <Text style={ styles.text }>
              РРО: { stock.rso }
            </Text>
          </View>
          { this.renderTouchable('rso') }
        </View>
        <View style={ styles.row }>
          <View>
            <Text style={ styles.text }>
              Наличные: { stock.cash }
            </Text>
          </View>
          { this.renderTouchable('cash') }
        </View>
      </View>
    );
  }
}

DisbursementButtons.propTypes = {
  shift: PropTypes.object,
  stock: PropTypes.object,
};

const styles = StyleSheet.create({
  container: {
    marginLeft: 25,
    marginBottom: 10,
  },
  row: {
    width: 280,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  header: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  text: {
    fontSize: 16,
  },
  touchable: {
    fontWeight: 'bold',
    color: '#1a9b9b',
  }
});
