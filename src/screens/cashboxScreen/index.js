import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, ActivityIndicator } from 'react-native';
import { Icon } from 'react-native-elements';
import { headerStyle, headerTitleStyle } from '../../styles/stackNavigator';
import { defaultColor } from '../../styles/palette';
import { centerFlexStyle } from '../../styles/common';
import { getRSO, getShift, getStock, setShift } from '../../actions/cashbox_actions';
import { updateProcessModal } from '../../actions/modals_actions';
import { SpinnerModal } from '../../components/SpinnerModal';
import { HeaderMenu } from '../../components/HeaderMenu';
import { HeaderServiceMenu } from '../../components/HeaderServiceMenu';
import { InfoBlock } from './InfoBlock';
import { ServiceMenu } from './ServiceMenu';
import { OpenShiftButton } from './OpenShiftButton';
import { CloseShiftButton } from './CloseShiftButton';
import { DisbursementArea } from './disbursementArea';

class CashboxScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { user, onPressMenu } = navigation.state.params || {};

    return {
      title: 'Касса',
      headerStyle: { ...headerStyle },
      headerTitleStyle: { ...headerTitleStyle },
      headerLeft: HeaderMenu(navigation),
      headerRight: user && user.isCashier
        ? <HeaderServiceMenu onPressMenu={ onPressMenu } />
        : <View />,
      drawerIcon: ({ tintColor }) => <Icon name='monetization-on' size={26} color={tintColor} />
    };
  };

  constructor(props) {
    super(props);

    this.state = {
      serviceMenuVisible: false,
    };

    this.handleOpenServiceMenu = this.handleOpenServiceMenu.bind(this);
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onPressMenu: this.handleOpenServiceMenu,
      user: this.props.user,
    });

    this.props.getRSO();
    this.props.getShift();
    this.props.getStock();
  }

  handleOpenServiceMenu() {
    this.setState(state => ({
      serviceMenuVisible: !state.serviceMenuVisible
    }));
  }

  renderServiceMenu(BTAddress) {
    if (!BTAddress) return null;

    const { serviceMenuVisible } = this.state;

    return (
      <ServiceMenu
        visible={ serviceMenuVisible }
        onVisible={ this.handleOpenServiceMenu }
        BTAddress={ BTAddress }
      />
    );
  }

  render() {
    const { isFetching, rso, shift, stock, user, processModal } = this.props;
    const BTAddress = rso && rso.RSO.BTAddress;

    if (!user) {
      return null;
    }

    if (isFetching === true) {
      return (
        <ActivityIndicator size='large' style={ centerFlexStyle } />
      );
    }

    return (
      <View>
        <View style={styles.buttonArea}>
          <OpenShiftButton
            style={ styles.buttonStyle }
            color={ defaultColor }
            disabled={ shift !== null }
            user={ user }
            openShift={ this.props.setShift }
            updateProcessModal={ this.props.updateProcessModal }
            rso={ rso }
          />
          <CloseShiftButton
            style={ styles.buttonStyle }
            color={ defaultColor }
            disabled={ shift === null }
            user={ user }
            closeShift={ this.props.setShift }
            updateProcessModal={ this.props.updateProcessModal }
            rso={ rso }
          />
        </View>
        <DisbursementArea
          rso={ rso }
          stock={ stock }
          shift={ shift }
          courier={ user.courier }
          getStock={ this.props.getStock }
          updateProcessModal={ this.props.updateProcessModal }
        />
        <InfoBlock
          rso={ rso }
          shift={ shift }
        />
        { this.renderServiceMenu(BTAddress) }
        <SpinnerModal { ...processModal } />
      </View>
    );
  }
}

CashboxScreen.propTypes = {
  rso: PropTypes.shape({
    RSO: PropTypes.shape({
      code: PropTypes.string,
      name: PropTypes.string,
      serialNumber: PropTypes.string,
      BTAddress: PropTypes.string,
    }),
    courier: PropTypes.string,
    dateReceived: PropTypes.string,
  }),
  shift: PropTypes.shape({
    name: PropTypes.string,
    courier: PropTypes.string,
    dateOpened: PropTypes.string,
  })
};

const styles = {
  buttonArea: {
    marginTop: 20,
    marginBottom: 20,
  },
  buttonStyle: {
    borderRadius: 5,
    margin: 5,
  },
  textArea: {
    marginLeft: 25,
  },
  cashboxBalance: {
    fontWeight: 'bold',
  }
};

function mapStateToProps({ auth, cashbox, modals }) {
  return {
    user: auth.currentUser,
    rso: cashbox.rso,
    shift: cashbox.shift,
    stock: cashbox.stock,
    isFetching: cashbox.isFetching,
    processModal: modals.processModal,
  };
}

const mapDispatchToProps = {
  getRSO,
  getShift,
  getStock,
  setShift,
  updateProcessModal,
};

export default connect(mapStateToProps, mapDispatchToProps)(CashboxScreen);
