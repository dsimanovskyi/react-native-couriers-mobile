import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { signoutUser } from '../actions/auth_actions';
import { headerStyle, headerTitleStyle } from '../styles/stackNavigator';
import { HeaderMenu } from '../components/HeaderMenu';

class ProfileScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Профиль',
      headerStyle: { ...headerStyle },
      headerTitleStyle: { ...headerTitleStyle },
      headerLeft: HeaderMenu(navigation),
      headerRight: <View />,
      drawerIcon: ({ tintColor }) => <Icon name='person' size={25} color={tintColor} />
    };
  };

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'flex-end', marginBottom: 10 }}>
        <Button
          title='Выйти'
          onPress={this.props.signoutUser}
        />
      </View>
    );
  }
}

function mapStateToProps({ auth }) {
  return {
    authenticated: auth.authenticated
  };
}

export default connect(mapStateToProps, { signoutUser })(ProfileScreen);
