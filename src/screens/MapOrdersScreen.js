import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import dateFormat from 'dateformat';
import MapView from 'react-native-maps';
import { Icon } from 'react-native-elements';
import { defaultColor } from '../styles/palette';
import { borderShadowStyle } from '../styles/common';
import { signoutUser } from '../actions/auth_actions';
import { getOrders, updateOrder } from '../actions/orders_actions';
import { getLocationAsync, watchPosition, setRegion } from '../actions/geolocation_actions';
import ModalMapIS from '../components/ModalMapIS';
import OrdersOnMap from '../components/OrdersOnMap';
import MyLocationMapMarker from '../components/MyLocationMapMarker';
import { formateOnlyDate } from '../constants/defaultDateFormate';
import updateList from '../utils/updateList';
import { getCourierStatusesByType } from '../helpers/getCourierStatusesByType';

class MapOrdersScreen extends Component {
  static navigationOptions = {
    title: 'Карта',
    header: null,
    tabBarIcon: ({ tintColor }) => <Icon name="place" size={25} color={tintColor} />,
    drawerIcon: ({ tintColor }) => <Icon name="place" size={25} color={tintColor} />
  };

  constructor(props) {
    super(props);
    const currentIS = this.props.navigation.state.params
                        ? this.props.navigation.state.params.currentIS
                        : null;

    this.currentIS = currentIS;
  }

  state = {
    isModalVisible: false,
    isSequenceMode: false,
    sequenceOrders: [],
    orders: [],
    showMap: false,
  };

  componentWillMount() {
    if (this.props.currentUser) {
      this.props.getOrders(this.props.currentUser.courier);
      this.props.watchPosition();
    }
  }

  componentDidMount() {
    const { navigation } = this.props;

    navigation.addListener('willFocus', () => {
      this.setState({ showMap: true });
    });

    navigation.addListener('didBlur', () => {
      this.setState({ showMap: false });
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setFilteredOrders(nextProps);
  }

  componentWillUnmount() {
    if (this.props.navigation.state.params) {
      this.props.navigation.setParams({ currentIS: null });
    }
  }

  onRegionChangeComplete = (region) => {
    this.props.setRegion(region);
  }

  onPressRefresh() {
    this.props.getOrders(this.props.currentUser.courier);
  }

  onPressGetCurrentLocation() {
    const { latitude, longitude } = this.props.location.coords;

    this.map.animateCamera({ center: { latitude, longitude } });
    this.props.setRegion({ ...this.props.region, latitude, longitude });
  }

  onPressOpenDrawer() {
    this.props.navigation.openDrawer();
  }

  onPressMarker(number) {
    if (this.state.isSequenceMode) {
      const sequenceOrders = [...this.state.sequenceOrders, number];
      this.setState({ sequenceOrders });
    } else {
      const screenOrder = this.currentIS ? 'OrderIS' : 'OrderIndex';
      this.props.navigation.navigate(screenOrder, { number });
    }
  }

  setFilteredOrders(nextProps) {
    const currentIS = this.currentIS;

    let listOrders = nextProps.listOrders.filter(order =>
      !order.courierStatus || order.courierStatus.isCompleted === false
    );

    if (currentIS) {
      listOrders = listOrders.filter(
        order => currentIS.ordersArray.find(orderIS => orderIS.order === order._id)
      );
    }
    // Отберем те по которым еще не установили последовательность
    listOrders = listOrders.filter(
      order => !this.state.sequenceOrders.find(orderNum => orderNum === order.number)
    );
    listOrders = this.setSequenceOrders(listOrders);
    this.setState({ orders: listOrders });
  }

  setSequenceOrders(listOrders) {
    const sortedOrders = [...listOrders];
    sortedOrders.sort((a, b) => a.deliverySequence < b.deliverySequence ? -1 : 0);
    const sequenceOrders = [];
    let sequenceOrder = 0;
    for (let i = 0; i < sortedOrders.length; i++) {
      const order = sortedOrders[i];
      if (order.deliverySequence) {
        sequenceOrder++;
        sequenceOrders.push({ _id: order._id, sequenceOrder });
      }
    }
    const newList = updateList(sortedOrders, '_id', sequenceOrders);
    return newList;
  }

  undoSequenceOrder() {
    const lengthArray = this.state.sequenceOrders.length;
    const sequenceOrders = this.state.sequenceOrders.slice(0, lengthArray - 1);
    this.setState({ sequenceOrders });
  }

  resetSequenceOrders() {
    const body = this.state.orders.filter(order => order.deliverySequence).map(order => (
      { _id: order._id, deliverySequence: null }
    ));
    this.props.updateOrder('', this.props.listOrders, body);
    this.setState({ sequenceOrders: [] });
  }

  saveSequenceOrders() {
    const body = this.state.sequenceOrders.map((orderNum, sequence) => {
      const foundOrder = this.props.listOrders.find(order => orderNum === order.number);
      return {
        deliverySequence: sequence + 1,
        _id: foundOrder._id
      };
    });
    this.props.updateOrder('', this.props.listOrders, body);
    this.setState({ isSequenceMode: false, sequenceOrders: [] });
  }

  _toggleModal() {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  }

  _setSequenceOrders() {
    this.setState({ isSequenceMode: true, isModalVisible: false });
  }

  _setStartIS() {
    const body = this.state.orders.map(order => ({
      _id: order._id,
      courierStatus: getCourierStatusesByType('on_way')[0]
    }));

    this.props.updateOrder('', this.props.listOrders, body);

    if (this.state.isModalVisible) {
      this.setState({
        isModalVisible: !this.state.isModalVisible
      });
    }
  }

  renderMarkers() {
    if (this.state.showMap !== true) return null;

    return (
      <React.Fragment>
        <OrdersOnMap
          listOrders={this.state.orders}
          onPressMarker={this.onPressMarker.bind(this)}
        />
        <MyLocationMapMarker
          coordinate={this.props.coordinate}
          location={this.props.location}
        />
      </React.Fragment>
    );
  }

  render() {
    const currentIS = this.currentIS;

    const interval = currentIS && currentIS.interval ? currentIS.interval : '<Не указан>';

    return (
      <View style={styles.container}>
        <MapView
          provider={this.props.provider}
          ref={ref => { this.map = ref; }}
          initialRegion={this.props.region}
          style={styles.map}
          onRegionChangeComplete={this.onRegionChangeComplete}
        >
          { this.renderMarkers() }
        </MapView>
        <View style={styles.buttonContainer}>
          {
            this.state.isSequenceMode ?
            <View>
              <Icon
                raised
                reverse
                onPress={this.undoSequenceOrder.bind(this)}
                name="undo"
                color={defaultColor}
              />
              <Icon
                raised
                reverse
                onPress={this.resetSequenceOrders.bind(this)}
                name="close"
                color={defaultColor}
              />
              <Icon
                raised
                reverse
                onPress={this.saveSequenceOrders.bind(this)}
                name="save"
                color={defaultColor}
              />
            </View>
            :
            <View>
              <Icon
                raised
                reverse
                onPress={this.onPressRefresh.bind(this)}
                name="refresh"
                color={defaultColor}
              />
              <Icon
                raised
                reverse
                onPress={this.onPressGetCurrentLocation.bind(this)}
                name="my-location"
                color={defaultColor}
              />
            </View>
          }
        </View>
        <View style={styles.menuContainer}>
          <Icon
            raised
            onPress={this.onPressOpenDrawer.bind(this)}
            name="menu"
          />
          {
            currentIS ?
            <TouchableOpacity onPress={this._toggleModal.bind(this)}>
              <View style={[styles.numberIS, borderShadowStyle]}>
                <Text>{currentIS.number}</Text>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.infoTextISStyle}>
                    {dateFormat(currentIS.deliveryDate, formateOnlyDate)}
                  </Text>
                  <Text style={styles.infoTextISStyle}>
                    ({interval})
                  </Text>
                  <Text style={styles.infoTextISStyle}>
                    ({this.state.orders.length})
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            :
            null
          }
        </View>
        <ModalMapIS
          isModalVisible={this.state.isModalVisible}
          _toggleModal={this._toggleModal.bind(this)}
          _setSequenceOrders={this._setSequenceOrders.bind(this)}
          _setStartIS={this._setStartIS.bind(this)}
        />
      </View>
    );
  }
}

MapOrdersScreen.propTypes = {
  provider: MapView.ProviderPropType,
};

function mapStateToProps({ auth, geolocation, orders }) {
  return {
    authenticated: auth.authenticated,
    currentUser: auth.currentUser,
    listOrders: orders.list,
    region: geolocation.region,
    regionAnimated: geolocation.regionAnimated,
    location: geolocation.location,
    coordinate: geolocation.coordinate,
  };
}

export default connect(mapStateToProps, { signoutUser,
                                          getLocationAsync,
                                          getOrders,
                                          updateOrder,
                                          watchPosition,
                                          setRegion })(MapOrdersScreen);

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    backgroundColor: 'transparent',
    ...StyleSheet.absoluteFillObject,
  },
  buttonContainer: {
    position: 'absolute',
    bottom: 10,
    right: 10,
  },
  menuContainer: {
    flex: 1,
    flexDirection: 'row',
    position: 'absolute',
    top: 30,
    left: 20,
  },
  numberIS: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 7,
    marginHorizontal: 15,
    paddingHorizontal: 20,
    paddingVertical: 5,
  },
  infoTextISStyle: {
    fontWeight: 'bold',
    paddingLeft: 3
  }
});
