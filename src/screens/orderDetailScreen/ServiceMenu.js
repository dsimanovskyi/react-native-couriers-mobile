import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Text,
  StyleSheet,
} from 'react-native';
import alertError from '../../components/AlertError';
import { ExellioCommandExecutor as Exellio } from '../../exellioDriver/ExellioCommandExecutor';
import { popMenuBackgroundColor, textTitleColorDefault } from '../../styles/palette';
import { DataProvider } from '../../helpers/DataProvider';

export class ServiceMenu extends Component {
  constructor(props) {
    super(props);

    this.exellio = Exellio(props.BTAddress);
    this.statusDelivered = this.getStatusDelivered();

    this.handleDismiss = this.handleDismiss.bind(this);
    this.handleCancelCheck = this.handleCancelCheck.bind(this);
    this.handleCompleteOrder = this.handleCompleteOrder.bind(this);
  }

  getStatusDelivered() {
    const courierStatuses = DataProvider().courierStatuses;

    return courierStatuses.filter(
      status => status.statusType === 'delivered'
    )[0];
  }

  handleCancelCheck() {
    this.handleDismiss();

    const cmd = { name: 'cancelFiscalCheck', data: '' };

    this.exellio.run(cmd)
      .then(report => {
        if (report.status === false) {
          alertError(report.error, report.errorCode);
        }
      });
  }

  handleCompleteOrder() {
    this.handleDismiss();

    const { order, onGiveOrder } = this.props;

    const data = {
      courierStatus: this.statusDelivered._id,
      sum: order.sum,
      owners: order.owners,
      returnedProducts: order.returnedProducts || [],
    };

    onGiveOrder(order._id, data, this.statusDelivered);
  }

  handleDismiss() {
    this.props.onVisible();
  }

  cancelCheckButton() {
    return (
      <View style={ styles.item }>
        <TouchableOpacity
          onPress={ this.handleCancelCheck }
        >
          <Text style={ styles.itemText }>
            Аннулировать чек
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  completeOrderButton() {
    const { courierStatus, paymentSum, sum } = this.props.order;

    if ((courierStatus && courierStatus.isCompleted === true) || paymentSum === sum) {
      return null;
    }

    return (
      <View style={ styles.item }>
        <TouchableOpacity
          onPress={ this.handleCompleteOrder }
        >
          <Text style={ styles.itemText }>
            Выдать без чека
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    const { visible } = this.props;

    return (
      <Modal
        transparent
        visible={ visible }
        onRequestClose={ this.handleDismiss }
      >
        <TouchableWithoutFeedback
          onPress={ this.handleDismiss }
        >
          <View style={ styles.container }>
            <View style={ styles.content }>
              { this.cancelCheckButton() }
              { this.completeOrderButton() }
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

ServiceMenu.propTypes = {
  visible: PropTypes.bool,
  BTAddress: PropTypes.string.isRequired,
  order: PropTypes.object,
  onVisible: PropTypes.func,
  onGiveOrder: PropTypes.func,
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
  },
  content: {
    right: 10,
    top: 40,
    position: 'absolute',
    minWidth: 250,
    backgroundColor: popMenuBackgroundColor,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    alignItems: 'center',
    overflow: 'hidden'
  },
  item: {
    width: '100%',
    alignItems: 'center',
    paddingVertical: 15,
  },
  itemText: {
    fontSize: 17,
    color: textTitleColorDefault,
  }
});
