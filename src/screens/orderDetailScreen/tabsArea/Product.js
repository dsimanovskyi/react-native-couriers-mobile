import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View } from 'react-native';
import Swipeout from 'react-native-swipeout';
import { textColorDefault } from '../../../styles/palette';
import { ProductActionForm } from './ProductActionForm';

export class Product extends Component {
  constructor(props) {
    super(props);

    this.state = {
      closeActionForm: true
    };

    this.handleAction = this.handleAction.bind(this);
  }

  handleAction(text) {
    const { product } = this.props;

    this.props.onAction({
      productId: product._id,
      skuId: product.sku._id,
      quantity: Number(text),
    });

    this.setState({
      closeActionForm: true
    });
  }

  render() {
    const { product, actionName } = this.props;

    if (!product.quantity) return null;

    const nameProduct = `${product.product.name} - ${product.sku.name}`;

    return (
      <Swipeout
        key={ product.idSKU }
        buttonWidth={ 150 }
        close={ this.state.closeActionForm }
        right={[
          {
            component: (
              <ProductActionForm
                text={ actionName }
                onAction={ this.handleAction }
              />
            ),
            backgroundColor: '#cc545a',
          }
        ]}
      >
        <View style={[styles.styleCard, styles.styleRow]}>
          <View style={{ flex: 7, alignItems: 'flex-start' }}>
            <Text style={styles.styleTextDefault}>{ nameProduct }</Text>
          </View>
          <View style={{ flex: 1, alignItems: 'flex-end' }}>
            <Text style={styles.styleTextDefault}>{ product.quantity }</Text>
          </View>
          <View style={{ flex: 2, alignItems: 'flex-end' }}>
            <Text style={styles.styleTextDefault}>{ Math.round(product.sum) } ₴</Text>
          </View>
        </View>
      </Swipeout>
    );
  }
}

Product.propTypes = {
  product: PropTypes.object,
  actionName: PropTypes.string,
  onAction: PropTypes.func,
};

const styles = StyleSheet.create({
  styleCard: {
    marginBottom: 10,
    marginHorizontal: 10,
  },
  styleRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  styleTextDefault: {
    color: textColorDefault,
  }
});
