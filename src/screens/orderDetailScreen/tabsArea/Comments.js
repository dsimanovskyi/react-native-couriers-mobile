import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';
import { textColorDefault } from '../../../styles/palette';

export class Comments extends Component {
  render() {
    return (
      <Text style={styles.styleTextDefault}>
        Здесь будет комментарий для курьера
      </Text>
    );
  }
}

const styles = StyleSheet.create({
  styleTextDefault: {
    color: textColorDefault,
  }
});
