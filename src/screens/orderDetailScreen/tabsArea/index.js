import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import Tabs from '../../../components/common/Tabs';
import { Order } from '../../../helpers/Order';
import { Products } from './Products';
import { Comments } from './Comments';

export class TabsArea extends Component {
  constructor(props) {
    super(props);

    this._order = new Order(props.order);

    this.handleReturnProduct = this.handleReturnProduct.bind(this);
    this.handleCancelReturnProduct = this.handleCancelReturnProduct.bind(this);
  }

  handleReturnProduct(productData) {
    const { courier, onReturnProducts } = this.props;
    const { productId, skuId, quantity } = productData;

    this._order.returnProduct(courier, productId, skuId, quantity);

    const order = this._order.getOrder();

    onReturnProducts({
      id: order._id,
      sum: order.sum,
      owners: order.owners,
      returnedProducts: order.returnedProducts,
      returnedProductList: order.returnedProductList,
    });
  }

  handleCancelReturnProduct(productData) {
    const { courier, onCancelReturnProducts } = this.props;
    const { productId, skuId, quantity } = productData;

    this._order.cancelReturnProduct(courier, productId, skuId, quantity);

    const order = this._order.getOrder();

    onCancelReturnProducts({
      id: order._id,
      sum: order.sum,
      owners: order.owners,
      returnedProducts: order.returnedProducts,
      returnedProductList: order.returnedProductList,
    });
  }

  render() {
    return (
      <Tabs>
        <View title='Товары' style={ styles.styleCard }>
          <Products
            products={ this._order.productsBySku }
            actionName='Возврат'
            onAction={ this.handleReturnProduct }
          />
        </View>
        <View title='Возврат' style={ styles.styleCard }>
          <Products
            products={ this._order.returnedProductsBySku }
            actionName='Отмена возврата'
            onAction={ this.handleCancelReturnProduct }
          />
        </View>
        <View title='Комментарии' style={ styles.styleCard }>
          <Comments />
        </View>
      </Tabs>
    );
  }
}

TabsArea.propTypes = {
  courier: PropTypes.string,
  order: PropTypes.object,
  onReturnProducts: PropTypes.func,
};

const styles = StyleSheet.create({
  styleCard: {
    marginBottom: 10,
    marginHorizontal: 10,
  },
});
