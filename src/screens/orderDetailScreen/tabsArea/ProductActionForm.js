import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import { NumberInput } from '../../../components/common/NumberInput';

export class ProductActionForm extends Component {
  constructor(props) {
    super(props);

    this.text = '';

    this.handlePress = this.handlePress.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handlePress() {
    this.props.onAction(this.text);
  }

  handleChange(text) {
    this.text = text;
  }

  render() {
    const { text } = this.props;

    return (
      <TouchableOpacity
        onPress={ this.handlePress }
        style={ styles.container }
      >
        <NumberInput
          style={ styles.input }
          placeholder='0'
          onChange={ this.handleChange }
        />
        <Text style={ styles.label }>
          { text }
        </Text>
      </TouchableOpacity>
    );
  }
}

ProductActionForm.propTypes = {
  text: PropTypes.string,
  onAction: PropTypes.func,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  input: {
    backgroundColor: '#fff',
    textAlign: 'center',
    width: 50
  },
  label: {
    color: '#fff',
  },
});
