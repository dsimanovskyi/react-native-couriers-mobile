import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { v1 as uuid } from 'uuid';
import { Product } from './Product';

export class Products extends Component {
  render() {
    const { products } = this.props;

    return products.map(product => (
      <Product
        key={ uuid() }
        product={ product }
        { ...this.props }
      />
    ));
  }
}

Products.propTypes = {
  products: PropTypes.array,
  actionName: PropTypes.string,
  onAction: PropTypes.func,
};

Products.defaultProps = {
  products: [],
};
