import React, { Component } from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import {
  updateOrder,
  cancelOrder,
  returnProducts,
  cancelReturnProducts,
  completeOrder,
} from '../../actions/orders_actions';
import { getStock } from '../../actions/cashbox_actions';
import { updateProcessModal } from '../../actions/modals_actions';
import { SpinnerModal } from '../../components/SpinnerModal';
import { headerStyle, headerTitleStyle } from '../../styles/stackNavigator';
import { Message } from '../../components/Message';
import { HeaderServiceMenu } from '../../components/HeaderServiceMenu';
import { MapArea } from './MapArea';
import { DateSumArea } from './DateSumArea';
import { ContactsArea } from './ContactsArea';
import { ButtonsArea } from './buttonsArea';
import { TabsArea } from './tabsArea';
import { ServiceMenu } from './ServiceMenu';
import { CancelOrderModal } from './CancelOrderModal';

class OrderDetailScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { number, user, onPressMenu } = navigation.state.params;

    return {
      title: number,
      headerStyle: { ...headerStyle },
      headerTitleStyle: { ...headerTitleStyle },
      headerRight: user && user.isCashier
        ? <HeaderServiceMenu onPressMenu={ onPressMenu } />
        : <View />,
    };
  };

  constructor(props) {
    super(props);

    this.state = {
      serviceMenuVisible: false,
      cancelOrderMenuVisible: false,
    };

    this.handleOpenServiceMenu = this.handleOpenServiceMenu.bind(this);
    this.handleOpenCancelOrderModal = this.handleOpenCancelOrderModal.bind(this);
    this.handleCancelOrder = this.handleCancelOrder.bind(this);
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onPressMenu: this.handleOpenServiceMenu,
      user: this.props.user,
    });
  }

  getCurrentOrder() {
    const { navigation, listOrders } = this.props;
    const { number } = navigation.state.params;

    return listOrders.find(
      order => order.number === number
    );
  }

  handleOpenServiceMenu() {
    this.setState(state => ({
      serviceMenuVisible: !state.serviceMenuVisible
    }));
  }

  handleOpenCancelOrderModal() {
    this.setState(state => ({
      cancelOrderMenuVisible: !state.cancelOrderMenuVisible
    }));
  }

  handleCancelOrder(status) {
    this.setState(state => ({
      cancelOrderMenuVisible: !state.cancelOrderMenuVisible
    }));

    const currentOrder = this.getCurrentOrder();

    this.props.cancelOrder(currentOrder._id, status);
  }

  renderServiceMenu(BTAddress, order) {
    if (!BTAddress) return null;

    const { serviceMenuVisible } = this.state;

    return (
      <ServiceMenu
        visible={ serviceMenuVisible }
        BTAddress={ BTAddress }
        order={ order }
        onVisible={ this.handleOpenServiceMenu }
        onGiveOrder={ this.props.giveOrder }
      />
    );
  }

  renderCancelOrderMenu() {
    const { cancelOrderMenuVisible } = this.state;

    return (
      <CancelOrderModal
        visible={ cancelOrderMenuVisible }
        onRequestClose={ this.handleOpenCancelOrderModal }
        onCancelOrder={ this.handleCancelOrder }
      />
    );
  }

  render() {
    const { location, rso, shift, user, processModal } = this.props;

    if (!shift) {
      return (
        <Message message='Для работы над заказом необходимо открыть смену' />
      );
    }

    const currentOrder = this.getCurrentOrder();

    return (
      <ScrollView>
        <View style={styles.container}>
          <MapArea order={ currentOrder } />
          <DateSumArea order={ currentOrder } />
          <ContactsArea
            order={ currentOrder }
            location={ location }
          />
          <ButtonsArea
            order={ currentOrder }
            BTAddress={ rso.RSO.BTAddress }
            user={ user }
            onCompleteOrder={ this.props.completeOrder }
            getStock={ this.props.getStock }
            onGiveCheck={ this.props.giveCheck }
            onGiveOrder={ this.props.giveOrder }
            onCancelOrder={ this.handleOpenCancelOrderModal }
            updateProcessModal={ this.props.updateProcessModal }
          />
          <TabsArea
            courier={ user.courier }
            order={ currentOrder }
            onReturnProducts={ this.props.returnProducts }
            onCancelReturnProducts={ this.props.cancelReturnProducts }
          />
        </View>
        { this.renderServiceMenu(rso.RSO.BTAddress, currentOrder) }
        { this.renderCancelOrderMenu() }
        <SpinnerModal { ...processModal } />
      </ScrollView>
    );
  }
}

function mapStateToProps({ auth, geolocation, orders, cashbox, modals }) {
  return {
    authenticated: auth.authenticated,
    location: geolocation.location,
    listOrders: orders.list,
    rso: cashbox.rso,
    shift: cashbox.shift,
    user: auth.currentUser,
    isUpdating: orders.isUpdating,
    processModal: modals.processModal,
  };
}

export default connect(mapStateToProps, {
  updateOrder,
  returnProducts,
  cancelReturnProducts,
  cancelOrder,
  completeOrder,
  updateProcessModal,
  getStock,
})(OrderDetailScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
});
