import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import MapView from 'react-native-maps';
import OrdersOnMap from '../../components/OrdersOnMap';

export class MapArea extends Component {
  getInitialRegion(coordinates) {
    return {
      longitude: +coordinates.lng,
      latitude: +coordinates.lat,
      latitudeDelta: 0.045,
      longitudeDelta: 0.02
    };
  }

  render() {
    const { order } = this.props;

    return (
      <View style={{ height: 150, marginBottom: 10 }}>
        <MapView
          style={{ flex: 1 }}
          initialRegion={ this.getInitialRegion(order.coordinates) }
        >
          <OrdersOnMap listOrders={ [order] } />
        </MapView>
      </View>
    );
  }
}

MapArea.propTypes = {
  order: PropTypes.object.isRequired,
};
