import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { phonecall } from 'react-native-communications';
import { Icon } from 'react-native-elements';
import getDirections from 'react-native-google-maps-directions';
import { defaultColor, textColorDefault } from '../../styles/palette';

export class ContactsArea extends Component {
  constructor(props) {
    super(props);
    
    this.handleGetDirections = this.handleGetDirections.bind(this);
  }

  handleGetDirections() {
    const { order, location } = this.props;
    const { latitude, longitude } = location.coords;
    const { coordinates } = order;

    const dataDirections = {
      source: { latitude, longitude },
      destination: { latitude: +coordinates.lat, longitude: +coordinates.lng },
      params: [
        {
          key: 'travelmode',
          value: 'driving'
        },
        {
          key: 'dir_action',
          value: 'navigate'
        }
      ]
    };

    getDirections(dataDirections);
  }
  
  render() {
    const { order } = this.props;

    return (
      <React.Fragment>
        <View style={[styles.styleCard, styles.styleRow]}>
          <View style={{ flex: 5 }}>
            <Text style={styles.styleTextDefault}>
              {order.client.name}
            </Text>
            <Text style={styles.styleTextDefault}>тел. {order.phone}</Text>
          </View>
          <View style={styles.styleIconView}>
            <TouchableOpacity onPress={() => { phonecall(order.phone, true); }}>
              <Icon
                size={ 35 }
                color={defaultColor}
                name='phone'
              />
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={this.handleGetDirections}
        >
          <View style={[styles.styleCard, styles.styleRow, { paddingRight: 5 }]}>
            <Icon
              containerStyle={styles.styleDirections}
              color={textColorDefault}
              name='directions'
            />
            <Text style={styles.styleTextDefault}>{order.address}</Text>
          </View>
        </TouchableOpacity>
      </React.Fragment>
    );
  }
}

ContactsArea.propTypes = {
  order: PropTypes.object,
  location: PropTypes.object,
};

const styles = StyleSheet.create({
  styleCard: {
    marginBottom: 10,
    marginHorizontal: 10,
  },
  styleRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  styleTextDefault: {
    color: textColorDefault,
  },
  styleDirections: {
    marginRight: 5
  },
  styleIconView: {
    flex: 1,
    marginRight: 5,
    alignItems: 'flex-end',
    justifyContent: 'flex-end'
  },
});
