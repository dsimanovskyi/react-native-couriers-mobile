import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View } from 'react-native';
import dateFormat from 'dateformat';
import { formateOnlyDate } from '../../constants/defaultDateFormate';

export class DateSumArea extends Component {
  render() {
    const { order } = this.props;
    const interval = order.interval || 'Не указан';

    return (
      <View style={ [styles.styleCard, styles.styleRow] }>
        <Text style={ styles.styleTextInfo }>
          { dateFormat(order.date, formateOnlyDate) } ({ interval })
        </Text>
        <View style={{ flex: 1, alignItems: 'flex-end' }}>
          <Text
            style={ styles.styleTextInfo }
          >
            { order.sum - order.paymentSum } ₴
          </Text>
        </View>
      </View>
    );
  }
}

DateSumArea.propTypes = {
  order: PropTypes.object,
};

const styles = StyleSheet.create({
  styleCard: {
    marginBottom: 10,
    marginHorizontal: 10,
  },
  styleRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
