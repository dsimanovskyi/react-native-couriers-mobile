import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { RadioFormModal } from '../../components/RadioFormModal';
import { DataProvider } from '../../helpers/DataProvider';

export class CancelOrderModal extends Component {
  constructor(props) {
    super(props);

    const reasons = DataProvider().courierStatuses;
    const reasonsData = this.reformatCancelReasons(reasons);
    const initialData = {
      initial: [
        { label: 'Не доставлен', value: 'not_delivered' },
        { label: 'Отказ', value: 'refusal' },
      ],
    };

    this.screenData = Object.assign(initialData, reasonsData);

    this.initialOptions = {
      step: 0,
      screen: 'initial',
      cancelOrderReason: null,
    };

    this.initialState = {
      content: this.screenData.initial,
      resetRadio: false,
      buttonBackIsDisabled: true,
      buttonFurtherIsDisabled: true,
    };

    this.options = Object.assign({}, this.initialOptions);
    this.state = Object.assign({}, this.initialState);

    this.handlePressRadio = this.handlePressRadio.bind(this);
    this.handlePressBack = this.handlePressBack.bind(this);
    this.handlePressFurther = this.handlePressFurther.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this);
  }

  reformatCancelReasons(reasons) {
    const reasonsData = {
      refusal: [],
      not_delivered: [],
    };

    reasons.forEach(reason => {
      const { statusType, name } = reason;

      if (reasonsData[statusType]) {
        reasonsData[statusType].push({
          label: name,
          value: reason,
        });
      }
    });

    return reasonsData;
  }

  handlePressRadio(value) {
    //todo: dosim - почему-то при нажатии на кнопки "Назад" или "Далее" вызывается этот обработчик
    if (!value) return;

    if (this.options.step === 0) {
      this.options.screen = value;
    } else {
      this.options.cancelOrderReason = value;
    }

    this.setState({
      buttonFurtherIsDisabled: false,
      resetRadio: false,
    });
  }

  handlePressBack() {
    if (this.options.step === 1) {
      this.options.step--;
      this.options.screen = 'initial';

      this.setState({
        content: this.screenData.initial,
        buttonBackIsDisabled: true,
        buttonFurtherIsDisabled: false,
        resetRadio: true,
      });
    }
  }

  handlePressFurther() {
    const { onCancelOrder } = this.props;

    if (this.options.step === 0) {
      this.options.step++;

      this.setState({
        content: this.screenData[this.options.screen],
        buttonBackIsDisabled: false,
        buttonFurtherIsDisabled: true,
        resetRadio: true,
      });
    } else if (this.options.step === 1) {
      onCancelOrder(this.options.cancelOrderReason);

      this.options = Object.assign({}, this.initialOptions);
      this.setState(this.initialState);
    }
  }

  handleRequestClose() {
    this.props.onRequestClose();

    this.options = Object.assign({}, this.initialOptions);
    this.setState(this.initialState);
  }

  render() {
    const { content, buttonBackIsDisabled, buttonFurtherIsDisabled, resetRadio } = this.state;
    const { visible } = this.props;

    return (
      <RadioFormModal
        radio={ content }
        resetRadio={ resetRadio }
        visible={ visible }
        onPressRadio={ this.handlePressRadio }
        onPressBack={ this.handlePressBack }
        onPressFurther={ this.handlePressFurther }
        onRequestClose={ this.handleRequestClose }
        buttonBackIsDisabled={ buttonBackIsDisabled }
        buttonFurtherIsDisabled={ buttonFurtherIsDisabled }
      />
    );
  }
}

CancelOrderModal.propTypes = {
  cancelReasons: PropTypes.array,
  visible: PropTypes.bool,
  onRequestClose: PropTypes.func,
  onCancelOrder: PropTypes.func,
};
