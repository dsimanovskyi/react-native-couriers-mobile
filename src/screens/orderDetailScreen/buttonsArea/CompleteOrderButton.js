import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Alert,
} from 'react-native';
import { DataProvider } from '../../../helpers/DataProvider';
import { nonFiscalCheckTemplate } from '../../../checkTemplates/nonFiscalCheckTemplate';
import { ExellioCommandExecutor as Exellio } from '../../../exellioDriver/ExellioCommandExecutor';
import { RequestManager } from '../../../helpers/RequestManager';
import alertError from '../../../components/AlertError';
import { PRINT_NON_FISCAL_CHECK, REQUEST_TO_API } from '../../../constants/messages';

export class CompleteOrderButton extends Component {
  constructor(props) {
    super(props);

    this.copiesNumber = 2;
    this.statusDelivered = this.getStatusDelivered();

    this.handleCompleteOrder = this.handleCompleteOrder.bind(this);
  }

  getStatusDelivered() {
    const courierStatuses = DataProvider().courierStatuses;

    return courierStatuses.filter(
      status => status.statusType === 'delivered'
    )[0];
  }

  handleCompleteOrder() {
    Alert.alert(
      'Обработка заказа',
      'Выдать заказ клиенту',
      [
        {
          text: 'Нет',
        },
        {
          text: 'Да',
          onPress: () => this.completeOrder()
        }
      ]
    );
  }

  async completeOrder() {
    const { order, user, onCompleteOrder, getStock, updateProcessModal } = this.props;

    const orderData = {
      courierStatus: this.statusDelivered._id,
      owners: order.owners,
      returnedProducts: order.returnedProducts,
      sum: order.sum,
    };

    try {
      if (order.paymentSum !== order.sum) {
        updateProcessModal({
          message: PRINT_NON_FISCAL_CHECK,
          visible: true,
        });

        // const report = await this.printCheck(); //todo: разобраться в чем причина ошибки
        const report = { status: true };

        if (report.status === true) {
          updateProcessModal({ message: REQUEST_TO_API });

          await this.doPaymentRequest(order, user);

          await onCompleteOrder(order._id, orderData, this.statusDelivered);
          await getStock();

          updateProcessModal({ visible: false });
        } else {
          updateProcessModal({ visible: false });

          const errorReport = {
            code: report.errorCode,
            user: user.courier,
            type: 'RSO',
            title: report.error,
            description: report,
          };

          RequestManager.logError(errorReport);
          alertError(report.error, report.errorCode);
        }
      } else {
        updateProcessModal({
          message: REQUEST_TO_API,
          visible: true,
        });

        await onCompleteOrder(order._id, orderData, this.statusDelivered);

        updateProcessModal({ visible: false });
      }
    } catch (e) {
      console.error(e);
      updateProcessModal({ visible: false });
    }
  }

  printCheck() {
    const { order, BTAddress } = this.props;
    const cmds = [];

    order.owners.forEach(owner => {
      const nonFiscalCheckData = nonFiscalCheckTemplate(order, owner);

      for (let i = 1; i <= this.copiesNumber; i++) {
        // const requestForTransferData = requestForTransferTemplate(order, owner, i);

        cmds.push({ name: 'nonFiscalCheck', data: nonFiscalCheckData });
        // cmds.push({ name: 'nonFiscalCheck', data: requestForTransferData });
      }
    });

    this.exellio = Exellio(BTAddress);

    return this.exellio.run(cmds);
  }

  doPaymentRequest(order, user) {
    return RequestManager.payment({
      courier: user.courier,
      order: order._id,
      sum: order.sum - order.paymentSum,
      paymentType: user.isCashier ? 'RSO' : 'cash',
    });
  }

  render() {
    return (
      <TouchableOpacity
        style={ styles.button }
        onPress={ this.handleCompleteOrder }
      >
        <Text style={ styles.text }>Выдать заказ</Text>
      </TouchableOpacity>
    );
  }
}

CompleteOrderButton.propTypes = {
  order: PropTypes.object.isRequired,
  BTAddress: PropTypes.string,
  user: PropTypes.object,
  onCompleteOrder: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  button: {
    flex: 1,
    height: 40,
    backgroundColor: '#68b79f',
    alignItems: 'center',
    padding: 8,
  },
  text: {
    fontSize: 17,
    fontWeight: 'bold',
    color: '#fff',
  }
});
