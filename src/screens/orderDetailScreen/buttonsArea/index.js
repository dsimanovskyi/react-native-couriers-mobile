import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import { CancelOrderButton } from './CancelOrderButton';
import { CompleteOrderButton } from './CompleteOrderButton';

export class ButtonsArea extends Component {
  render() {
    const {
      order,
      user,
      BTAddress,
      onCompleteOrder,
      getStock,
      onCancelOrder,
      updateProcessModal
    } = this.props;

    if (order.courierStatus && order.courierStatus.isCompleted === true) {
      return null;
    }

    return (
      <View style={ styles.container }>
        <CompleteOrderButton
          order={ order }
          BTAddress={ BTAddress }
          user={ user }
          onCompleteOrder={ onCompleteOrder }
          getStock={ getStock }
          updateProcessModal={ updateProcessModal }
        />
        <CancelOrderButton
          onCancelOrder={ onCancelOrder }
          updateProcessModal={ updateProcessModal }
        />
      </View>
    );
  }
}

ButtonsArea.propTypes = {
  order: PropTypes.object.isRequired,
  BTAddress: PropTypes.string,
  user: PropTypes.object,
  onCancelOrder: PropTypes.func.isRequired,
  onCompleteOrder: PropTypes.func.isRequired,
  updateProcessModal: PropTypes.func,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  }
});
