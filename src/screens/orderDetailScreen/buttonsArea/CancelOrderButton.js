import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity
} from 'react-native';

export class CancelOrderButton extends Component {
  render() {
    const { onCancelOrder } = this.props;

    return (
      <TouchableOpacity
        style={ styles.button }
        onPress={ onCancelOrder }
      >
        <Text style={ styles.text }>Отменить заказ</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    flex: 1,
    height: 40,
    backgroundColor: '#bf8180',
    alignItems: 'center',
    padding: 8,
  },
  text: {
    fontSize: 17,
    fontWeight: 'bold',
    color: '#fff',
  }
});
