import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, Platform, ActivityIndicator } from 'react-native';
import { FormValidationMessage, Button } from 'react-native-elements';
import { signInUser, authError } from '../actions/auth_actions';
import { getRSO, getShift, getStock } from '../actions/cashbox_actions';
import { Input } from '../components/common';
import { defaultColor } from '../styles/palette';
import { DataProvider } from '../helpers/DataProvider';

class AuthScreen extends Component {
  static navigationOptions = {
    headerStyle: {
      marginTop: Platform.OS === 'android' ? 24 : 0
    },
    tabBarLabel: 'Авторизация',
    title: 'Авторизация',
    tabBarVisible: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      username: '', password: ''
    };
  }

  handleFormSubmit() {
    const { username, password } = this.state;
    this.props.signInUser({ username, password })
      .then(authenticated => {
        if (authenticated === true) {
          this.props.getRSO();
          this.props.getShift();
          this.props.getStock();
          DataProvider();
        }
      });
  }

  render() {
    const { containerStyle, logoStyle, textLogoStyle,
          textStyle, buttonContainer, buttonStyle } = styles;

    return (
      <View style={containerStyle}>

        <View style={logoStyle}>
          <Text style={textLogoStyle}>PARFUMS.UA</Text>
          <Text style={textStyle}>магазин красивых покупок</Text>
        </View>

        <Input
          placeholder="Введите логин"
          label="Логин"
          onChangeText={username => this.setState({ username })}
        />
        <Input
          secureTextEntry
          placeholder="Введите пароль"
          label="Пароль"
          onChangeText={password => this.setState({ password })}
        />
        {this.props.fetchAuth && <ActivityIndicator />}
        <FormValidationMessage>{this.props.errorMessage}</FormValidationMessage>
        <Button
          large
          buttonStyle={buttonStyle}
          containerViewStyle={buttonContainer}
          icon={{ name: 'done' }}
          backgroundColor={defaultColor}
          onPress={this.handleFormSubmit.bind(this)}
          title='ВОЙТИ'
        />
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoStyle: {
    marginBottom: 30
  },
  textLogoStyle: {
    fontSize: 25,
    color: defaultColor
  },
  textStyle: {
    fontSize: 12
  },
  buttonContainer: {
    alignSelf: 'stretch',
    marginTop: 20,
    borderRadius: 5
  },
  buttonStyle: {
    borderRadius: 5
  }
};

function mapStateToProps({ auth }) {
  return { errorMessage: auth.error, fetchAuth: auth.fetchAuth };
}

export default connect(mapStateToProps, { signInUser, authError, getRSO, getShift, getStock })(AuthScreen);
