import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ActivityIndicator } from 'react-native';
import { getISs } from '../../actions/IS_actions';
import statusIS from '../../enum/statusIS';
import { List } from '../../components/common';
import { centerFlexStyle } from '../../styles/common';
import { headerStyle, headerTitleStyle } from '../../styles/stackNavigator';
import { HeaderMenu } from '../../components/HeaderMenu';
import { HeaderServiceMenu } from '../../components/HeaderServiceMenu';
import { ServiceMenu } from './ServiceMenu';

class Index extends Component {
  static navigationOptions = ({ navigation }) => {
    const { onPressMenu } = navigation.state.params || {};

    return {
      title: 'Марш. листы',
      headerStyle: { ...headerStyle },
      headerTitleStyle: { ...headerTitleStyle },
      headerLeft: HeaderMenu(navigation),
      headerRight: <HeaderServiceMenu onPressMenu={ onPressMenu } />,
    };
  };

  constructor(props) {
    super(props);

    this.state = {
      serviceMenuVisible: false,
    };

    this.handleOpenServiceMenu = this.handleOpenServiceMenu.bind(this);
    this.handleRefreshList = this.handleRefreshList.bind(this);
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onPressMenu: this.handleOpenServiceMenu,
    });

    if (this.props.currentUser) {
      this.props.getISs(this.props.currentUser.courier);
    }
  }

  onPressList(number) {
    const currentIS = this.props.listIS.find(itemIS => itemIS.number === number);
    this.props.navigation.navigate('MapIS', { currentIS });
  }

  handleOpenServiceMenu() {
    this.setState(state => ({
      serviceMenuVisible: !state.serviceMenuVisible
    }));
  }

  handleRefreshList() {
    if (this.props.currentUser) {
      this.props.getISs(this.props.currentUser.courier);
    }

    this.setState({
      serviceMenuVisible: false,
    });
  }

  render() {
    if (this.props.isFetching) {
      return <ActivityIndicator size='large' style={centerFlexStyle} />;
    }

    const { serviceMenuVisible } = this.state;

    return (
      <React.Fragment>
        <List
          data={this.props.listIS}
          title='number'
          gorups={[
            {
              direction: 'row',
              fields: [
                {
                  name: 'deliveryDate',
                  type: 'date'
                },
                {
                  name: 'interval',
                  type: 'string'
                },
                {
                  name: 'quantityOrders',
                  type: 'string'
                },
                {
                  name: 'deliveryArea.name',
                  type: 'string',
                  style: {
                    alignItems: 'flex-end',
                  }
                }
              ]
            }
          ]}
          filter='number'
          groupBy='status'
          groupByData={statusIS}
          onPressList={this.onPressList.bind(this)}
        />
        <ServiceMenu
          visible={ serviceMenuVisible }
          onVisible={ this.handleOpenServiceMenu }
          onRefreshList={ this.handleRefreshList }
        />
      </React.Fragment>
    );
  }
}

function mapStateToProps({ auth, ISs }) {
  return {
    listIS: ISs.list,
    isFetching: ISs.isFetching,
    currentUser: auth.currentUser
  };
}

export default connect(mapStateToProps, { getISs })(Index);
