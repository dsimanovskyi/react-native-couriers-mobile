import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Text,
  StyleSheet,
} from 'react-native';
import { popMenuBackgroundColor, textTitleColorDefault } from '../../styles/palette';

export class ServiceMenu extends Component {
  constructor(props) {
    super(props);

    this.handleDismiss = this.handleDismiss.bind(this);
  }

  handleDismiss() {
    this.props.onVisible();
  }

  render() {
    const { visible, onRefreshList } = this.props;

    return (
      <Modal
        transparent
        visible={ visible }
        onRequestClose={ this.handleDismiss }
      >
        <TouchableWithoutFeedback
          onPress={ this.handleDismiss }
        >
          <View style={ styles.container }>
            <View style={ styles.content }>
              <View style={ styles.item }>
                <TouchableOpacity
                  onPress={ onRefreshList }
                >
                  <Text style={ styles.itemText }>
                    Обновить список
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

ServiceMenu.propTypes = {
  visible: PropTypes.bool,
  onRefreshList: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
  },
  content: {
    right: 10,
    top: 40,
    position: 'absolute',
    minWidth: 250,
    backgroundColor: popMenuBackgroundColor,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    alignItems: 'center',
    overflow: 'hidden'
  },
  item: {
    width: '100%',
    alignItems: 'center',
    paddingVertical: 15,
  },
  itemText: {
    fontSize: 17,
    color: textTitleColorDefault,
  }
});
