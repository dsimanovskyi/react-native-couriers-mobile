import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ActivityIndicator, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { v1 as uuid } from 'uuid';
import { getOrders } from '../actions/orders_actions';
import intervals from '../enum/intervals';
import { List } from '../components/common';
import { centerFlexStyle } from '../styles/common';
import { headerStyle, headerTitleStyle } from '../styles/stackNavigator';
import { HeaderMenu } from '../components/HeaderMenu';

class OrdersListScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Заказы',
      headerStyle: { ...headerStyle },
      headerTitleStyle: { ...headerTitleStyle },
      headerLeft: HeaderMenu(navigation),
      headerRight: <View />,
      drawerIcon: ({ tintColor }) => <Icon name="shopping-cart" size={25} color={tintColor} />
    };
  };

  componentDidMount() {
    if (this.props.currentUser) {
      this.props.getOrders(this.props.currentUser.courier);
    }
  }

  onPressList(number) {
    this.props.navigation.navigate('OrderDetail', { number, refresh: this.refresh });
  }

  filterOrders(orders) {
    return orders.filter(order => {
      const { courierStatus } = order;

      return !courierStatus || courierStatus.isCompleted === false;
    });
  }

  render() {
    const { isFetching, listOrders } = this.props;

    if (isFetching) {
      return (
        <ActivityIndicator size='large' style={centerFlexStyle} />
      );
    }

    const orders = this.filterOrders(listOrders);

    return (
      <List
        key={ uuid() }
        data={orders}
        title='number'
        gorups={[
          {
            direction: 'row',
            fields: [
              {
                name: 'shippingDate',
                type: 'date',
                style: {
                  flex: 1,
                }
              },
              {
                name: 'interval',
                type: 'string',
                style: {
                  flex: 1,
                }
              },
              {
                name: 'sum',
                type: 'price',
                style: {
                  flex: 2,
                  alignItems: 'flex-end',
                },
                styleText: {
                  fontWeight: 'bold'
                }
              },
            ]
          },
          {
            direction: 'column',
            fields: [
              {
                name: 'client.name',
                type: 'string'
              },
              {
                name: 'phone',
                type: 'string'
              },
              {
                name: 'address',
                type: 'string'
              }
            ]
          }
        ]}
        filter='number'
        groupBy='interval'
        groupByData={intervals}
        onPressList={this.onPressList.bind(this)}
      />
    );
  }
}

function mapStateToProps({ auth, orders }) {
  return {
    listOrders: orders.list,
    isFetching: orders.isFetching,
    currentUser: auth.currentUser
  };
}

export default connect(mapStateToProps, { getOrders })(OrdersListScreen);
