export default (error, codeError = '') => {
  const messageError = error.response && error.response.data && error.response.data.error ?
                    error.response.data.error : 'Ошибка авторизации! Обратитесь к администратору!';

  const codeErrorMessage = codeError ? `:${codeError}` : '';

  if (!messageError) {
    return `Ошибка авторизации! Обратитесь к администратору${codeErrorMessage}`;
  } else if (typeof messageError === 'object' && messageError.message) {
    return `${messageError.message}${codeErrorMessage}`;
  } else if (typeof messageError === 'string') {
    return `${messageError}${codeErrorMessage}`;
  }
};
