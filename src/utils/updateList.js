export default (listSource, key, body, valueKey = null) => {
  const arrBody = body.length || body.length === 0 ? body : [body];
  let newList = [...listSource]; //.filter(() => true);
  for (let i = 0; i < arrBody.length; i++) {
    const currentBody = arrBody[i];
    const value = currentBody[key] ? currentBody[key] : valueKey;
    const updateIndex = newList.findIndex((row) => row[key] === value);
    const updateRow = newList[updateIndex];
    newList = [...newList.slice(0, updateIndex),
                  { ...updateRow, ...currentBody },
                   ...newList.slice(updateIndex + 1)];
  }
  return newList;
};
