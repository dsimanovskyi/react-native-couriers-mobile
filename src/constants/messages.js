export const REQUEST_TO_API = 'Выполнение запроса к API...';
export const REQUEST_TO_RSO = 'Выполнение запроса к РРО...';
export const PRINT_X_REPORT = 'Печать X отчета...';
export const PRINT_Z_REPORT = 'Печать Z отчета...';
export const PRINT_NON_FISCAL_CHECK = 'Печать чека...';
