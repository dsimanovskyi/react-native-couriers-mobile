import config from '../config';

const ROOT_URL = config.get('apiURL');

export const URL = {
  getRSO: () => `${ROOT_URL}/cashbox/rso/couriers?closed=false`,
  openShift: () => `${ROOT_URL}/cashbox/shift/couriers?closed=false`,
  closeShift: () => `${ROOT_URL}/cashbox/shift/couriers?closed=true`,
  getShift: () => `${ROOT_URL}/cashbox/shift/couriers?closed=false`,
  errorLog: () => `${ROOT_URL}/logs/error`,
  payment: () => `${ROOT_URL}/cashbox/payment`,
  order: (id) => `${ROOT_URL}/orders/${id}`,
  courierStatuses: () => `${ROOT_URL}/couriers/statuses`,
  getStock: () => `${ROOT_URL}/cashbox/stock`,
  disbursement: () => `${ROOT_URL}/cashbox/disbursement`,
};
