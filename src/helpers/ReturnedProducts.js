import _ from 'lodash';

export class ReturnedProducts {
  constructor(items = []) {
    this._items = _.cloneDeep(items);
  }

  setItem(courierId, orderId, ownerId, productId, skuId, quantity) {
    const index = this.getItemIndex(ownerId, skuId);

    if (index === -1) {
      this._items.push({
        courier: courierId,
        order: orderId,
        owner: ownerId,
        product: productId,
        sku: skuId,
        quantity,
      });
    } else {
      const item = this._items[index];

      this._items[index] = {
        courier: courierId,
        order: orderId,
        owner: ownerId,
        product: productId,
        sku: skuId,
        quantity: item.quantity + quantity
      };
    }
  }

  getItemIndex(ownerId, skuId) {
    return this._items.findIndex(
      item => item.owner === ownerId && item.sku === skuId
    );
  }

  getItems() {
    return this._items.filter(
      item => item.quantity !== 0
    );
  }
}
