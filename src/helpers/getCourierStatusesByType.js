import { DataProvider } from './DataProvider';

export function getCourierStatusesByType(type) {
  const courierStatuses = DataProvider().courierStatuses;

  return courierStatuses.filter(
    status => status.statusType === type
  );
}
