import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import { URL } from '../constants/api';
import alertError from '../components/AlertError';

export class RequestManager {
  static async getRSO() {
    try {
      const url = URL.getRSO();
      const authOptions = await this._getAuthOptions();
      const response = await axios.get(url, authOptions);

      return { status: true, data: response.data };
    } catch (error) {
      return await this._handleError(error);
    }
  }

  static async getShift() {
    try {
      const url = URL.getShift();
      const authOptions = await this._getAuthOptions();
      const response = await axios.get(url, authOptions);

      return { status: true, data: response.data };
    } catch (error) {
      return await this._handleError(error);
    }
  }

  static async openShift() {
    try {
      const url = URL.openShift();
      const authOptions = await this._getAuthOptions();
      const response = await axios.post(url, {}, authOptions);

      return { status: true, data: response.data };
    } catch (error) {
      return await this._handleError(error);
    }
  }

  static async closeShift() {
    try {
      const url = URL.closeShift();
      const authOptions = await this._getAuthOptions();
      const response = await axios.post(url, {}, authOptions);

      return { status: true, data: response.data };
    } catch (error) {
      return await this._handleError(error);
    }
  }

  static async payment(data) {
    try {
      const url = URL.payment();
      const authOptions = await this._getAuthOptions();
      const response = await axios.post(url, data, authOptions);

      return { status: true, data: response.data };
    } catch (error) {
      return await this._handleError(error);
    }
  }

  static async updateOrder(id, data) {
    try {
      const url = URL.order(id);
      const authOptions = await this._getAuthOptions();
      const response = await axios.put(url, data, authOptions);

      return { status: true, data: response.data };
    } catch (error) {
      return await this._handleError(error);
    }
  }

  static async getCourierStatuses() {
    try {
      const url = URL.courierStatuses();
      const authOptions = await this._getAuthOptions();
      const response = await axios.get(url, authOptions);

      return { status: true, data: response.data };
    } catch (error) {
      return await this._handleError(error);
    }
  }

  static async getStock() {
    try {
      const url = URL.getStock();
      const authOptions = await this._getAuthOptions();
      const response = await axios.get(url, authOptions);

      return { status: true, data: response.data };
    } catch (error) {
      return await this._handleError(error);
    }
  }

  static async disbursement(data) {
    try {
      const url = URL.disbursement();
      const authOptions = await this._getAuthOptions();
      const response = await axios.post(url, data, authOptions);

      return { status: true, data: response.data };
    } catch (error) {
      return await this._handleError(error);
    }
  }

  static async logError(errorReport) {
    try {
      const url = URL.errorLog();
      const authOptions = await this._getAuthOptions();
      const response = await axios.post(url, errorReport, authOptions);

      return { status: true, data: response.data };
    } catch (error) {
      return { status: false, error: error.response };
    }
  }

  static async _getAuthOptions() {
    const token = await AsyncStorage.getItem('token');

    return {
      headers: { authorization: token }
    };
  }

  static async _handleError(error) {
    const errorReport = await this._makeErrorReport(error);

    this.logError(errorReport);
    this._showErrorMessage(error);

    return { status: false, error: error.response };
  }

  static _showErrorMessage(error) {
    const { response } = error;
    const message = response.data.message;
    const code = response.status;

    alertError(message, code);
  }

  static async _makeErrorReport(error) {
    const { response } = error;
    const user = await AsyncStorage.getItem('user');

    return {
      code: response.status,
      user,
      type: 'Server',
      title: response.data.message,
      description: response.error
    };
  }
}
