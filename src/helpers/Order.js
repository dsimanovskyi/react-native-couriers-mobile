import _ from 'lodash';
import { Owner } from './Owner';
import { ReturnedProducts } from './ReturnedProducts';
import { ReturnedProductList } from './ReturnedProductList';

export class Order {
  constructor(order) {
    this._order = _.cloneDeep(order);
    this._owners = this._getOwners();
    this._returnedProducts = new ReturnedProducts(this._order.returnedProducts);
    this._returnedProductList = new ReturnedProductList(this._order.returnedProductList);
  }

  get id() {
    return this._order._id;
  }

  get productsBySku() {
    let products = [];

    this._owners.forEach(owner => {
      products = products.concat(owner.products);
    });

    const groups = _.groupBy(products, product => product.skuId);

    return Object.keys(groups).map(
      key => this._uniteProducts(groups[key])
    );
  }

  get returnedProductsBySku() {
    return this._returnedProductList.getItems();
  }

  returnProduct(courierId, productId, skuId, quantity) {
    const ownersHasProduct = this._owners.filter(
      owner => owner.hasProduct(skuId) === true
    );

    for (let i = 0; i < ownersHasProduct.length; i++) {
      const owner = ownersHasProduct[i];
      const ownerProductQuantity = owner.getProductQuantity(skuId);
      const quantityCanBeReturned = Math.min(ownerProductQuantity, quantity);

      owner.returnProduct(skuId, quantityCanBeReturned);

      this._returnedProducts.setItem(courierId, this._order._id, owner.id, productId, skuId, quantityCanBeReturned);
      this._returnedProductList.setItem(owner.getProduct(skuId).getProduct(), quantityCanBeReturned);

      quantity -= quantityCanBeReturned;

      if (quantity === 0) break;
    }

    this._calculateSum();
  }

  cancelReturnProduct(courierId, productId, skuId, quantity) {
    const ownersHasProduct = this._owners.filter(
      owner => owner.hasProduct(skuId) === true
    );

    for (let i = 0; i < ownersHasProduct.length; i++) {
      const owner = ownersHasProduct[i];
      const ownerProductQuantity = owner.getProductQuantity(skuId);
      const ownerProductMaxQuantity = owner.getProductMaxQuantity(skuId);
      const quantityCanBeCanceled = Math.min(ownerProductMaxQuantity - ownerProductQuantity, quantity);

      owner.cancelReturnProduct(skuId, quantityCanBeCanceled);

      this._returnedProducts.setItem(courierId, this._order._id, owner.id, productId, skuId, -quantityCanBeCanceled);
      this._returnedProductList.setItem(owner.getProduct(skuId).getProduct(), -quantityCanBeCanceled);

      quantity -= quantityCanBeCanceled;

      if (quantity === 0) break;
    }

    this._calculateSum();
  }

  getOrder() {
    const order = _.cloneDeep(this._order);

    order.owners = this._owners.map(
      owner => owner.getOwner()
    );

    order.returnedProducts = this._returnedProducts.getItems();
    order.returnedProductList = this._returnedProductList.getItems();

    return order;
  }

  _getOwners() {
    return this._order.owners.map(owner => {
      return new Owner(owner);
    });
  }

  _calculateSum() {
    this._order.sum = this._owners.reduce(
      (sum, owner) => sum += owner.sum,
      0
    );
  }

  _uniteProducts(products) {
    const quantity = products.reduce(
      (sum, product) => sum += product.quantity,
      0
    );

    const unitedProduct = products[0];
    unitedProduct.quantity = quantity;

    return unitedProduct.getProduct();
  }
}
