const arrNumbers = [
  [
    '',
    'один',
    'два',
    'три',
    'чьотири',
    'п\'ять',
    'шість',
    'сім',
    'вісім',
    'дев\'ять',
    'десять',
    'одинадцять',
    'дванадцять',
    'тринадцять',
    'чотирнадцять',
    'п\'ятнадцять',
    'шістнадцять',
    'сімнадцять',
    'вісімнадцять',
    'дев\'ятнадцять',
  ],
  [
    '',
    '',
    'двадцять',
    'тридцять',
    'сорок',
    'п\'ятдесят',
    'шістдесят',
    'сімдесят',
    'вісімдесят',
    'дев\'яносто',
  ],
  [
    '',
    'cто',
    'двісті',
    'триста',
    'чотириста',
    'п\'ятсот',
    'шістсот',
    'сімсот',
    'вісімсот',
    'дев\'ятсот'
  ],
];

const units = {
  1: ['тисяч', 'тисяча', 'тисячі'],
  2: ['мільйонів', 'мільйон', 'мільйона'],
  3: ['мільярдів', 'мільярд', 'мільярда'],
};

export function numberToString(number, toUpper = false) {
  if (number === undefined || typeof number !== 'number') {
    return false;
  }

  const numberParts = number.toFixed(2).split('.');
  const integer = numberParts[0];
  const fractional = numberParts[1];

  let string = '';
  let numParser = '';
  let count = 0;

  for (let p = integer.length - 1; p >= 0; p--) {
    const numDigit = integer.substr(p, 1);

    numParser = numDigit + numParser;

    if ((numParser.length == 3 || p == 0) && !isNaN(parseFloat(numParser))) {
      string = integerParser(numParser, count) + string;
      numParser = '';
      count++;
    }
  }

  if (fractional) {
    string += fractionalParser(fractional);
  }

  string = string.trim();
  string = string.replace(/\s{2,}/g, ' ');

  if (toUpper === true) {
    string = string.trim().substr(0,1).toUpperCase() + string.substr(1);
  }

  return string;
};

function integerParser(num, desc) {
  let string = '';
  let numHundred = '';
  let numTens = num;

  if (num.length === 3) {
    numHundred = num.substr(0, 1);
    numTens = num.substr(1, 3);
    string = ` ${arrNumbers[2][numHundred]} `;
  }

  if (numTens < 20) {
    string += `${arrNumbers[0][parseFloat(numTens)]} `;
  } else {
    const firstNum = numTens.substr(0, 1);
    const secondNum = numTens.substr(1, 2);

    string += `${arrNumbers[1][firstNum]} ${arrNumbers[0][secondNum]} `;
  }

  num = num.replace(/^[0]{1,}$/g, '0');

  string += getUnits(desc, num);

  if (desc === 0 || desc === 1) {
    string = string.replace('один ', 'одна ');
    string = string.replace('два ', 'дві ');
  }

  return string;
}

function fractionalParser(num) {
  const firstNum = num.substr(0, 1);
  const secondNum = parseFloat(num.substr(1, 2));

  return ` ${firstNum}${secondNum} коп.`;
}

function getUnits(desc, num, string = '') {
  if (desc === 0) {
    return 'грн.';
  }

  const unitsArr = units[desc];
  const lastNum = parseFloat(num.substr(-1));

  if (num.length === 2 && parseFloat(num.substr(0, 1)) === 1) {
    string += unitsArr[0];
  } else if (lastNum === 1) {
    string += unitsArr[1];
  } else if (lastNum > 1 && lastNum < 5) {
    string += unitsArr[2];
  } else if (parseFloat(num) > 0) {
    string += unitsArr[0];
  }

  return string;
}
