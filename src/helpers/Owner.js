import _ from 'lodash';
import { Product } from './Product';

export class Owner {
  constructor(owner) {
    this._owner = _.cloneDeep(owner);
    this._products = this._getProducts();
  }

  get id() {
    return this._owner._id;
  }

  get products() {
    return _.cloneDeep(this._products);
  }

  get sum() {
    return this._owner.sum;
  }

  getProduct(skuId) {
    return this._products.find(
      product => product.skuId === skuId
    );
  }

  getProductQuantity(skuId) {
    const product = this.getProduct(skuId);

    return product.quantity;
  }

  getProductMaxQuantity(skuId) {
    const product = this.getProduct(skuId);

    return product.maxQuantity;
  }

  hasProduct(skuId) {
    const index = this._products.findIndex(
      product => product.skuId === skuId
    );

    return index !== -1;
  }

  returnProduct(skuId, quantity) {
    const product = this.getProduct(skuId);

    product.decreaseQuantity(quantity);

    this._calculateSum();
  }

  cancelReturnProduct(skuId, quantity) {
    const product = this.getProduct(skuId);

    product.increaseQuantity(quantity);

    this._calculateSum();
  }

  getOwner() {
    const owner = _.cloneDeep(this._owner);

    owner.products = this._products.map(
      product => product.getProduct()
    );

    return owner;
  }

  _getProducts() {
    return this._owner.products.map(product => {
      return new Product(product);
    });
  }

  _calculateSum() {
    this._owner.sum = this._products.reduce(
      (sum, product) => sum += product.sum,
      0
    );
  }
}
