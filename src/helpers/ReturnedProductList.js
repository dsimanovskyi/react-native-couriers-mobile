import _ from 'lodash';

export class ReturnedProductList {
  constructor(items = []) {
    this._items = _.cloneDeep(items);
  }

  setItem(data, quantity) {
    const { _id, product, sku, price } = data;
    const index = this.getItemIndex(sku._id);

    if (index === -1) {
      this._items.push({
        _id,
        product,
        sku,
        sum: quantity * price,
        quantity,
      });
    } else {
      const item = this._items[index];
      const totalQuantity = item.quantity + quantity;

      this._items[index] = {
        _id,
        product,
        sku,
        sum: totalQuantity * price,
        quantity: totalQuantity,
      };
    }
  }

  getItemIndex(skuId) {
    return this._items.findIndex(
      item => item.sku._id === skuId
    );
  }

  getItems() {
    return this._items.filter(
      item => item.quantity !== 0
    );
  }
}
