import { _ } from 'lodash';
import { RequestManager } from './RequestManager';

class DataProviderSingleton {
  constructor() {
    this._getCourierStatuses();
  }

  get courierStatuses() {
    return _.cloneDeep(this._courierStatuses);
  }
  
  _getCourierStatuses() {
    RequestManager.getCourierStatuses()
      .then(response => {
        if (response.status === true) {
          this._courierStatuses = response.data;
        } else {
          this._courierStatuses = false;
        }
      });
  }
}

let instance = null;

export function DataProvider() {
  if (instance === null) {
    instance = new DataProviderSingleton();
  }

  return instance;
}

