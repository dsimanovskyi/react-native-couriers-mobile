import _ from 'lodash';

export class Product {
  constructor(product) {
    this._product = _.cloneDeep(product);
    this._maxQuantity = this._product.quantity;
  }

  set quantity(quantity) {
    this._product.quantity = quantity;
    this._calculateSum();
  }

  get quantity() {
    return this._product.quantity;
  }

  get maxQuantity() {
    return this._maxQuantity;
  }

  get skuId() {
    return this._product.sku._id;
  }

  get sum() {
    return this._product.sum;
  }

  increaseQuantity(quantity) {
    this.quantity = this._product.quantity + quantity;
  }

  decreaseQuantity(quantity) {
    const fixedQuantity = Math.min(this._product.quantity, quantity);
    this.quantity = this._product.quantity - fixedQuantity;
  }

  getProduct() {
    return _.cloneDeep(this._product);
  }

  _calculateSum() {
    const { price, quantity } = this._product;

    this._product.sum = price * quantity;
  }
}
