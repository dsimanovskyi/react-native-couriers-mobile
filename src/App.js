import React, { Component } from 'react';
import { ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { appContainer } from './routers';
import { initStateUser } from './actions/auth_actions';
import { getRSO, getShift, getStock } from './actions/cashbox_actions';
import { DataProvider } from './helpers/DataProvider';

class App extends Component {
  componentWillMount() {
    this.props.initStateUser()
      .then(() => {
        if (this.props.authenticated === true) {
          this.props.getRSO();
          this.props.getShift();
          this.props.getStock();
          DataProvider();
        }
      });
  }

  render() {
    if (this.props.fetchInitState) {
      return <ActivityIndicator size='large' style={styles.activityIndicatorStyle} />;
    }

    const RootAppLayout = appContainer(this.props.authenticated);

    return <RootAppLayout />;
  }
}

const styles = {
  activityIndicatorStyle: {
    flex: 1,
    justifyContent: 'center'
  }
};

function mapStateToProps({ auth }) {
  return { authenticated: auth.authenticated, fetchInitState: auth.fetchInitState };
}

export default connect(mapStateToProps, { initStateUser, getRSO, getShift, getStock })(App);
