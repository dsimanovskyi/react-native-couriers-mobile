import {AppRegistry} from 'react-native';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import App from './src/App';
import store from './src/store';
import {name as appName} from "./app.json";

class RootApp extends Component {
  render() {
    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}

AppRegistry.registerComponent(appName, () => RootApp);
